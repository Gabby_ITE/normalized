<?php

namespace App\Models\Tag;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    protected $fillable = [
        'name'
    ];

    public function books()
    {
        return $this->hasMany(BookTag::class);
    }
    public function series()
    {
        return $this->hasMany(SeriesTag::class);
    }
    public function clubs()
    {
        return $this->hasMany(ClubTag::class);
    }
}
