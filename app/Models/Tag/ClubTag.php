<?php

namespace App\Models\Tag;

use App\Models\Club\Club;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClubTag extends Model
{
    use HasFactory;

    protected $fillable = [
        'club_id',
        'tag_id',
    ];

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
    public function club()
    {
        return $this->belongsTo(Club::class);
    }
}
