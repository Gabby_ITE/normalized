<?php

namespace App\Models\Tag;

use App\Models\Book\Book;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookTag extends Model
{
    use HasFactory;

    protected $fillable = [
       'tag_id',
       'book_id',
    ];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
