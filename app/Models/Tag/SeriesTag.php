<?php

namespace App\Models\Tag;

use App\Models\Series\Series;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeriesTag extends Model
{
    use HasFactory;
    protected $fillable = [
        'tag_id',
        'series_id',
    ];

    public function series()
    {
        return $this->belongsTo(Series::class);
    }
    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
