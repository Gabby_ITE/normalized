<?php

namespace App\Models\Tag;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavoriteTag extends Model
{
    use HasFactory;
    protected $fillable = [
        'tag_id',
        'user_id',
     ];

     public function user()
     {
         return $this->belongsTo(User::class);
     }
     public function tag()
     {
         return $this->belongsTo(Tag::class);
     }
}
