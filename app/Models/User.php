<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Mail\VerifyEmail;
use App\Models\Author\FavoriteAuthor;
use App\Models\Book\BookHighLight;
use App\Models\Book\Bookmark;
use App\Models\Book\BookNote;
use App\Models\Book\BookRating;
use App\Models\Book\BookReview;
use App\Models\Book\BookStatus;
use App\Models\Challenge\ChallengeProgress;
use App\Models\Challenge\Challenge;
use App\Models\Club\ClubMember;
use App\Models\Request\BookRequest;
use App\Models\Request\ClubRequest;
use App\Models\Request\FriendRequest;
use App\Models\Request\Report;
use App\Models\Request\Response;
use App\Models\Series\SeriesReview;
use App\Models\Tag\FavoriteTag;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'image',
        'role',
        'password',
        'is_closed',
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail($this));
    }
    public function book_statuses()
    {
        return $this->hasMany(BookStatus::class,'user_id');
    }
    public function book_reviews()
    {
        return $this->hasMany(BookReview::class);
    }
    public function series_reviews()
    {
        return $this->hasMany(SeriesReview::class);
    }
    public function tags()
    {
        return $this->hasMany(FavoriteTag::class);
    }

    public function created_challenges()
    {
        return $this->hasMany(Challenge::class,'creator_id');
    }
    public function progressed_challenges()
    {
        return $this->hasMany(ChallengeProgress::class);
    }

    public function authors()
    {
        return $this->hasMany(FavoriteAuthor::class);
    }

    public function created_clubs()
    {
        return $this->hasMany(Club::class,'admin_id');
    }

    public function joined_clubs()
    {
        return $this->hasMany(ClubMember::class);
    }

    public function send_friend_requests()
    {
        return $this->hasMany(FriendRequest::class,'sender_id');
    }
    public function receive_friend_requests()
    {
        return $this->hasMany(FriendRequest::class,'receiver_id');
    }

    public function send_club_requests()
    {
        return $this->hasMany(ClubRequest::class,'sender_id');
    }
    public function receive_club_requests()
    {
        return $this->hasMany(ClubRequest::class,'receiver_id');
    }

    public function book_requests()
    {
        return $this->hasMany(BookRequest::class,'sender_id');
    }

    public function received_book_requests()
    {
        if ($this->role == 'admin'){
            return $this->hasMany(BookRequest::class,'receiver_id');
        }
        return null;
    }

    public function reports()
    {
        return $this->hasMany(Report::class,'sender_id');
    }

    public function received_reports()
    {
        return $this->hasMany(Report::class,'receiver_id');
    }

    public function send_responses()
    {
        return $this->hasMany(Response::class,'sender_id');
    }
    public function receive_responses()
    {
        return $this->hasMany(Response::class,'receiver_id');
    }

    // public function friends(User $other_user)
    // {
    //     return $this->sent_friends()->where('receiver_id', $other_user->id)
    //                 ->orWhere(function($query) use($other_user) {
    //                     $query->where('sender_id', $other_user->id)
    //                         ->where('status', 'accepted');
    //                 })
    //                 ->exists();
    // }

    public function friends()
    {
        return $this->hasMany(Friend::class, 'sender_id')
                    ->orWhere('receiver_id', $this->id)
                    ->union(
                        $this->hasMany(Friend::class, 'receiver_id')->orWhere('sender_id', $this->id)
                    );
    }

    public function notes()
    {
        return $this->hasMany(BookNote::class);
    }
    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

    public function logouts()
    {
        return $this->hasMany(Logout::class);
    }

    public function ratings()
    {
        return $this->hasMany(BookRating::class);
    }
}
