<?php

namespace App\Models\Challenge;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChallengeProgress extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'challenge_id',
        'acheived_number',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }

    public function scopeStatus($query, $challengeId)
    {
        return $query->where('challenge_id', $challengeId)
            ->where(function ($query) {
                $query->where('status', 'not_decide')
                    ->where(function ($query) {
                        $query->where('acheived_number', '<', $this->max_number)
                            ->orWhereNull('acheived_number');
                    })
                    ->where('end_date', '>', now());
            })
            ->orWhere(function ($query) {
                $query->where('status', 'finished')
                    ->orWhere(function ($query) {
                        $query->where('status', 'not_decide')
                            ->where(function ($query) {
                                $query->where('acheived_number', '>=', $this->max_number)
                                    ->where('end_date', '>', now());
                            });
                    });
            })
            ->orWhere(function ($query) {
                $query->where('status', 'timeout')
                    ->orWhere(function ($query) {
                        $query->where('status', 'not_decide')
                            ->where('end_date', '<=', now());
                    });
            });
    }

}
