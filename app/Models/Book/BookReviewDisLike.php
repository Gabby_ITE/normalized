<?php

namespace App\Models\Book;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookReviewDisLike extends Model
{
    use HasFactory;
    protected $fillable = [
        'book_review_id',
        'user_id'
    ];
}
