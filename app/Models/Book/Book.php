<?php

namespace App\Models\Book;

use App\Http\Resources\Author\ManyAuthorResource;
use App\Models\Author\AuthorBook;
use App\Models\Series\BookSeries;
use App\Models\Tag\BookTag;
use App\Models\User;
use Database\Factories\Book\BookFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $fillable=[
        'user_id',
        'title',
        'cover',
        'file',
        'number_of_pages',
        'release_date'
    ];

    protected $factories = BookFactory::class;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function authors()
    {
        return $this->hasMany(AuthorBook::class);
    }

    public function series()
    {
        return $this->hasOne(BookSeries::class);
    }

    public function ratings()
    {
        return $this->hasMany(BookRating::class);
    }

    public function statuses()
    {
        return $this->hasMany(BookStatus::class);
    }

    public function tags()
    {
        return $this->hasMany(BookTag::class);
    }
    public function reviews()
    {
        return $this->hasMany(BookReview::class);
    }
    public function discussions()
    {
        return $this->hasMany(BookDiscussion::class);
    }

    public function descriptions()
    {
        return $this->hasMany(BookDescription::class);
    }
    public function highLights()
    {
        return $this->hasMany(BookHighLight::class);
    }
    public function notes()
    {
        return $this->hasMany(BookNote::class);
    }
    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }
}
