<?php

namespace App\Models\Book;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookDiscussionReply extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'book_discussion_id',
        'book_discussion_reply_id',
        'text',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->hasMany(BookDiscussionReply::class);
    }
}
