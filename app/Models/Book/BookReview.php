<?php

namespace App\Models\Book;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookReview extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'book_id',
        'text',
        'is_burn',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes()
    {
        return $this->hasMany(BookReviewLike::class);
    }
    public function dis_likes()
    {
        return $this->hasMany(BookReviewDisLike::class);
    }
}
