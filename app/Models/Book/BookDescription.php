<?php

namespace App\Models\Book;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookDescription extends Model
{
    use HasFactory;
    protected $fillable =[
        'book_id',
        'text',
        'name',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
