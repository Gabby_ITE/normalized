<?php

namespace App\Models\Book;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookRating extends Model
{
    use HasFactory;
    // public $timestamps = false;
    protected $fillable = [
        'user_id',
        'book_id',
        'rating'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
