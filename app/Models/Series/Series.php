<?php

namespace App\Models\Series;

use App\Http\Resources\AuthorResource;
use App\Models\Author\AuthorSeries;
use App\Models\Tag\SeriesTag;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    use HasFactory;
    protected $fillable=[
        'title',
        'cover',
        'release_date'
    ];

    public function books()
    {
        return $this->hasMany(BookSeries::class);
    }
    public function authors()
    {
        // return AuthorResource::collection($this->hasMany(AuthorSeries::class));
        return $this->hasMany(AuthorSeries::class);
    }
    public function ratings()
    {
        return $this->hasMany(SeriesRating::class);
    }
    public function tags()
    {
        return $this->hasMany(SeriesTag::class);
    }

    public function reviews()
    {
        return $this->hasMany(SeriesReview::class);
    }
    public function discussions()
    {
        return $this->hasMany(SeriesDiscussion::class);
    }
    public function descriptions()
    {
        return $this->hasMany(SeriesDescription::class);
    }
}
