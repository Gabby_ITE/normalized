<?php

namespace App\Models\Series;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeriesRating extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'series_id',
        'rating'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
