<?php

namespace App\Models\Series;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeriesReviewDisLike extends Model
{
    use HasFactory;
    protected $fillable = [
        'series_review_id',
        'user_id'
    ];
}
