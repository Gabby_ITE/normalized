<?php

namespace App\Models\Series;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SeriesReview extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'series_id',
        'text',
        'is_burn',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function series()
    {
        return $this->BelongsTo(Series::class);
    }
    public function likes()
    {
        return $this->hasMany(SeriesReviewLike::class);
    }
    public function dis_likes()
    {
        return $this->hasMany(SeriesReviewDisLike::class);
    }
}
