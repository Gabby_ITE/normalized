<?php

namespace App\Models\Series;

use App\Http\Resources\AuthorResource;
use App\Models\Author\AuthorSeries;
use App\Models\Book\Book;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookSeries extends Model
{
    use HasFactory;
    protected $fillable = [
        'book_id',
        'series_id'
    ];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
