<?php

namespace App\Models\Series;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeriesDescription extends Model
{
    use HasFactory;
    protected $fillable =[
        'series_id',
        'text',
        'name',
    ];
}
