<?php

namespace App\Models\Series;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeriesDiscussionReply extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'series_discussion_id',
        'series_discussion_reply_id',
        'text'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function replies()
    {
        return $this->hasMany(SeriesDiscussionReply::class);
    }
}
