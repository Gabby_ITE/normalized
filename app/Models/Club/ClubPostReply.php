<?php

namespace App\Models\Club;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClubPostReply extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'club_post_id',
        'club_post_reply_id',
        'text',
    ];

    public function post(){
        return $this->belongsTo(ClubPost::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function replies()
    {
        return $this->hasMany(ClubPostReply::class);
    }
}
