<?php

namespace App\Models\Club;

use App\Models\Request\Report;
use App\Models\Tag\ClubTag;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    use HasFactory;
    protected $fillable = [
        'admin_id',
        'image',
        'name',
        'description',
        'is_private',
        'is_closed'
    ];

    public function admin()
    {
        return $this->belongsTo(User::class,'admin_id');
    }

    public function members()
    {
        return $this->hasMany(ClubMember::class);
    }

    public function tags()
    {
        return $this->hasMany(ClubTag::class);
    }

    public function posts()
    {
        return $this->hasMany(ClubPost::class);
    }
    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}
