<?php

namespace App\Models\Request;

use App\Models\Book\BookDiscussion;
use App\Models\Book\BookDiscussionReply;
use App\Models\Book\BookReview;
use App\Models\Club\ClubPost;
use App\Models\Club\ClubPostReply;
use App\Models\Series\SeriesDiscussion;
use App\Models\Series\SeriesDiscussionReply;
use App\Models\Series\SeriesReview;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;
    protected $fillable = [
        'sender_id',
        'receiver_id',
        'reported_type',
        'reported_id',
        'message',
        'is_done',
        'is_read',
    ];

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }
    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }

    public function reported()
    {
        if ($this->reported_type == 'series_review'){
            return $this->belongsTo(SeriesReview::class,'reported_id');
        }
        if ($this->reported_type == 'book_review'){
            return $this->belongsTo(BookReview::class,'reported_id');
        }
        if ($this->reported_type == 'book_discussion'){
            return $this->belongsTo(BookDiscussion::class,'reported_id');
        }
        if ($this->reported_type == 'book_discussion_reply'){
            return $this->belongsTo(BookDiscussionReply::class,'reported_id');
        }

        if ($this->reported_type == 'series_discussion'){
            return $this->belongsTo(SeriesDiscussion::class,'reported_id');
        }
        if ($this->reported_type == 'series_discussion_reply'){
            return $this->belongsTo(SeriesDiscussionReply::class,'reported_id');
        }
        if ($this->reported_type == 'club_post'){
            return $this->belongsTo(ClubPost::class,'reported_id');
        }
        else
            return $this->belongsTo(ClubPostReply::class,'reported_id');
    }
    public function response()
    {
        return $this->hasMany(Response::class,'request_id');
    }
}
