<?php

namespace App\Models\Request;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    use HasFactory;

    protected $fillable = [
        'sender_id',
        'receiver_id',
        'type',
        'request_type',
        'request_id',
        'is_read',
    ];

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }
    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }
    public function request(){
        if ($this->request_type == 'club'){
            return $this->belongsTo(ClubRequest::class, 'request_id');
        }
        if ($this->request_type == 'friend'){
            return $this->belongsTo(FriendRequest::class, 'request_id');
        }
        if ($this->request_type == 'report'){
            return $this->belongsTo(Report::class, 'request_id');
        }
        else{
            return $this->belongsTo(BookRequest::class, 'request_id');
        }


    }
}
