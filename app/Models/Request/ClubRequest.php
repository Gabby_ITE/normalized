<?php

namespace App\Models\Request;

use App\Models\User;
use App\Models\Club\Club;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClubRequest extends Model
{
    use HasFactory;
    protected $fillable = [
        'sender_id',
        'receiver_id',
        'club_id',
        'is_done',
        'is_read',
    ];

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id');
    }
    public function receiver()
    {
        return $this->belongsTo(User::class,'receiver_id');
    }
    public function club()
    {
        return $this->belongsTo(Club::class);
    }
    public function response()
    {
        return $this->hasMany(Response::class,'request_id');
    }
}
