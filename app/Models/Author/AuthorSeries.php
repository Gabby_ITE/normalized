<?php

namespace App\Models\Author;

use App\Models\Series\Series;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AuthorSeries extends Model
{
    use HasFactory;

    protected $fillable = [
        'author_id',
        'series_id',
    ];

    public function series()
    {
        return $this->BelongsTo(Series::class);
    }
    public function author()
    {
        return $this->belongsTo(Author::class);
    }

}
