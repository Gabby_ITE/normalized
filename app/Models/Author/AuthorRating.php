<?php

namespace App\Models\Author;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthorRating extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'author_id',
        'rating'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
