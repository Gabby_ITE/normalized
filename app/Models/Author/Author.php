<?php

namespace App\Models\Author;

use App\Models\Series\Series;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_name',
        'last_name',
        'image'
    ];

    public function books()
    {
        return $this->hasMany(AuthorBook::class);
    }
    public function series()
    {
        return $this->hasMany(AuthorSeries::class);
    }
    public function ratings()
    {
        return $this->hasMany(AuthorRating::class);
    }
    public function users()
    {
        return $this->hasMany(FavoriteAuthor::class);
    }
}
