<?php

namespace App\Models\Author;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FavoriteAuthor extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'author_id'
    ];

    public function author()
    {
        return $this->belongsTo(Author::class);
    }
}
