<?php

namespace App\Http\Resources\Challenge;

use App\Http\Resources\User\ManyUserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ManyChallengeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'creator'=>new ManyUserResource($this->creator),
            'name'=>$this->name,
            'type'=>$this->type,
            'max_number'=>$this->max_number,
            'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
        ];
    }
}
