<?php

namespace App\Http\Resources\Club;

use App\Http\Resources\User\ManyUserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleClubPostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'user'=>new ManyUserResource($this->user),
            'text'=>$this->text,
            'replies'=>SingleClubPostResource::collection($this->replies),
            'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
        ];
    }
}
