<?php

namespace App\Http\Resources\Club;

use App\Http\Resources\User\ManyUserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleClubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        if($this->is_closed){
            if ($request->user()->role == 'admin'
                || $this->members->where('user_id',$request->user()->id)->exists() == 1){
                    return  [
                        'id'=>$this->id,
                        'name'=>$this->name,
                        'image'=>$this->image,
                        'descritpion'=>$this->description,
                        'admin'=>new ManyUserResource($this->admin),
                        'members'=>ManyUserResource::collection($this->members->pluck('user')),
                        'number_of_members'=>$this->members->count(),
                        'posts'=>ManyClubPostResource::collection($this->posts),
                        'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                    ];
                }else{
                    return  [
                        'id'=>$this->id,
                        'name'=>$this->name,
                        'image'=>$this->image,
                        'descritpion'=>$this->description,
                        'admin'=>new ManyUserResource($this->admin),
                        'number_of_members'=>$this->members->count(),
                        'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                    ];
                }
        }else{
            return  [
                'id'=>$this->id,
                'name'=>$this->name,
                'image'=>$this->image,
                'descritpion'=>$this->description,
                'admin'=>new ManyUserResource($this->admin),
                'members'=>ManyUserResource::collection($this->members->pluck('user')),
                'number_of_members'=>$this->members->count(),
                'posts'=>ManyClubPostResource::collection($this->posts),
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }
    }
}
