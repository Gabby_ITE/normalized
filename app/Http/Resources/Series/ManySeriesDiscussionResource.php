<?php

namespace App\Http\Resources\Series;

use App\Http\Resources\User\ManyUserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ManySeriesDiscussionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'user'=>new ManyUserResource($this->user),
            'text'=>$this->text,
        ];
    }
}
