<?php

namespace App\Http\Resources\Series;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SeriesReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'user_name'=>$this->user->first_name .
                            ' ' . $this->user->last_name,
            'rating'=>$this->rate(),
            'text'=>$this->text,
            'is_burn'=>$this->is_burn,
            'your_like'=>$this->your_like($request),
            'likes'=>$this->likes()->count(),
            'your_dis_like'=>$this->your_dis_like($request),
            'dis_likes'=>$this->dis_likes()->count(),
        ];
    }

    private function your_like(Request $request)
    {
        return $this->likes()->where('user_id',$request->user()->id)->exists();
    }
    private function your_dis_like(Request $request)
    {
        return $this->dis_likes()->where('user_id',$request->user()->id)->exists();
    }
    private function rate()
    {

        $rate = $this->series->ratings()->where('user_id',$this->user_id)->get();
        if ($rate->count() > 0)
            return $rate[0]->rating;
        return null;
    }

}
