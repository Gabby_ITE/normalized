<?php

namespace App\Http\Resources\Series;

use App\Http\Resources\Author\ManyAuthorResource;
use App\Http\Resources\AuthorResource;
use App\Http\Resources\Book\ManyBookResource;
use App\Models\Series\Series;
use App\Models\Series\SeriesRating;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleSeriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'cover'=>$this->cover,
            'release_date'=>$this->release_date,
            // 'my_rating'=>$this->rate($request),
            'rating'=>$this->rating(),
            'authors'=>$this->getAuthors(),
            'books'=>$this->hasBooks(),
            'descriptions'=>SeriesDescriptionResource::collection($this->descriptions),
        ];
    }

    private function getAuthors()
    {
        $authors = $this->authors->pluck('author');
        return $authors;
    }
    private function rate(Request $request)
    {
        $rate = $this->ratings()->select('rating')
            ->where('user_id',$request->user()->id)
            ->get();
        return $rate[0]->rating;
    }

    private function  rating()
    {
        if ($this->hasMany(SeriesRating::class)->count()==0)
            return null;
        $ratings =  $this->hasMany(SeriesRating::class);
        if ($ratings->count() > 0){
            $sum = 0;
            foreach($ratings->select('rating')->get() as $rating){
                return $sum = $sum + $rating->rating;
            }
            $sum = $sum/$ratings->count();
            return $sum;
        }
    }

    public function hasBooks(){
        return ManyBookResource::collection(
            $this->books->pluck('book')->sortByDesc('release_date')
        );
    }
}
