<?php

namespace App\Http\Resources\Request;

use App\Http\Resources\User\ManyUserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FriendRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = null;
        if ($this->sender_id == $request->user()->id){
            $user = $this->receiver;
        }else if ($this->receiver_id == $request->user()->id){
            $user = $this->sender;
        }
        if ($this->is_done){
            return [
                'id'=>$this->id,
                'is_done'=>$this->is_done,
                'user'=>new ManyUserResource($user),
                'response'=>new ResponseResource($this->response->where('request_type','friend')->first()),
                'is_read'=>$this->is_read,
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }
        return [
            'id'=>$this->id,
            'is_done'=>$this->is_done,
            'user'=>new ManyUserResource($user),
            'is_read'=>$this->is_read
        ];

    }
}
