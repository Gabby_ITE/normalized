<?php

namespace App\Http\Resources\Request;

use App\Http\Resources\Club\ManyClubResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClubRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        if ($this->is_done){
            return [
                'id'=>$this->id,
                'is_done'=>$this->is_done,
                'club'=>new ManyClubResource($this->club),
                'response'=>new ResponseResource($this->response->where('request_type','club')->first()),
                'is_read'=>$this->is_read,
                'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
            ];
        }
        return [
            'id'=>$this->id,
            'is_done'=>$this->is_done,
            'club'=>new ManyClubResource($this->club),
            'is_read'=>$this->is_read,
            'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
        ];
    }
}
