<?php

namespace App\Http\Resources\Request;

use App\Http\Resources\Club\ManyClubResource;
use App\Http\Resources\User\ManyUserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = null;
        if ($this->sender_id == $request->user()->id){
            $user = $this->receiver;
        }else if ($this->receiver_id == $request->user()->id){
            $user = $this->sender;
        }
        if (!$user){
            return [];
        }
        // return [new ManyUserResource($user)];
        if($this->is_done){
            if ($this->reported_id){
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'message'=>$this->message,
                    'reported_type'=>$this->reported_type,
                    'reported'=>$this->reported,
                    'is_done'=>$this->is_done,
                    'response'=>new ResponseResource($this->response->where('request_type','report')->first()),
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }
            else{
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'title'=>$this->title,
                    'author_name'=>$this->author_name,
                    'is_done'=>$this->is_done,
                    'is_read'=>$this->is_read,
                    'response'=>new ResponseResource($this->response->where('request_type','book')->first()),
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }
        }else{
            if($this->reported_id){
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'reported_type'=>$this->reported_type,
                    'reported'=>$this->reported,
                    'message'=>$this->message,
                    'is_done'=>$this->is_done,
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }
            else{
                return [
                    'id'=>$this->id,
                    'user'=>new ManyUserResource($user),
                    'title'=>$this->title,
                    'author_name'=>$this->author_name,
                    'is_done'=>$this->is_done,
                    'is_read'=>$this->is_read,
                    'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
                ];
            }
        }

    }
}
