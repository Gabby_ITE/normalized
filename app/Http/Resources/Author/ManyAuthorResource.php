<?php

namespace App\Http\Resources\Author;

use App\Models\Author\AuthorRating;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ManyAuthorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
            'image'=>$this->image,
            'rating'=>$this->rating(),
        ];
    }

    private function  rating()
    {
        if (!$this->hasMany(AuthorRating::class)){
            return null;
        }
        $ratings =  $this->hasMany(AuthorRating::class);
        if ($ratings->count() != 0){
            $sum = 0;
            foreach($ratings->select('rating')->get() as $rating){
                return $sum = $sum + $rating->rating;
            }
            $sum = $sum/$ratings->count();
            return $sum;
        }
        // return 0;
    }
}
