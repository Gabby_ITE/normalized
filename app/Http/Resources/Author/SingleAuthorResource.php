<?php

namespace App\Http\Resources\Author;

use App\Http\Resources\Book\ManyBookResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleAuthorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
            'image'=>$this->image,
            'rating'=>$this->rate(),
            'your_rating'=>$this->user_rate($request),
            'books'=>ManyBookResource::collection($this->has_books()),
        ];
    }
    private function user_rate(Request $request)
    {
        $rate = $this->ratings()->select('rating')
            ->where('user_id',$request->user()->id)
            ->get();
        if ($rate->count())
            return $rate[0]->rating;
        return null;
    }

    private function has_books()
    {
        return $this->books()->get()->pluck('book');
    }
    private function rate()
    {
        $rate = $this->ratings()->get();
        if ($rate->count() == 0){
            return null;
        }
        $sum = 0;
        foreach($rate as $r){
            $sum += $r->rating;
        }
        $rate = $sum/$rate->count();
        return $rate;
    }
}
