<?php

namespace App\Http\Resources\Book;

use App\Http\Resources\Author\ManyAuthorResource;
use App\Http\Resources\TagResource;
use App\Models\Book\BookRating;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleBookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'cover'=>$this->cover,
            'file'=>$this->file,
            'release_date'=>$this->release_date,
            'my_rating'=>$this->rate($request),
            'rating'=>$this->rating(),
            'status'=>$this->status($request),
            'authors'=>$this->boook_authors(),
            'tags'=>$this->getTags(),
            'descriptions'=>BookDescriptionResource::collection($this->descriptions),
        ];
    }

    private function boook_authors(){
        try{
            return ManyAuthorResource::collection($this->authors->pluck('author'));
        }catch(Exception $e){
            return null;
        }
        // return null;
    }
    private function rate(Request $request)
    {
        $rate = $this->ratings()->select('rating')
            ->where('user_id',$request->user()->id)
            ->get();
        if ($rate->count())
            return $rate[0]->rating;
        return null;
    }
    private function status(Request $request)
    {
        $status = $this->statuses()
                        ->where('user_id',$request->user()->id)
                        ->get();
        if ($status->count())
            return [
                'id' =>$status[0]->id,
                'status' =>$status[0]->status,
                'progress'=>$status[0]->progress,
            ];
        return null;
    }
    private function rating()
    {
        try{
            $ratings =  $this->hasMany(BookRating::class);
            if ($ratings->count() != 0){
                $sum = 0;
                foreach($ratings->select('rating')->get() as $rating){
                    return $sum = $sum + $rating->rating;
                }
                $sum = $sum/$ratings->count();
                return $sum;
            }
        }
        catch (Exception $e){
            return 0;
        }
    }
    public function getTags()
    {
        return TagResource::collection($this->tags()->get()->pluck('tag'));
    }
}
