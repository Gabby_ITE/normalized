<?php

namespace App\Http\Resources\Book;

use App\Models\Book\BookRating;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ManyBookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'cover'=>$this->cover,
            'rating'=>$this->rating(),
        ];
    }
    private function rating()
    {
        if (!$this->hasMany(BookRating::class)){
            return null;
        }
        $ratings =  $this->hasMany(BookRating::class);
        if ($ratings->count() != 0){
            $sum = 0;
            foreach($ratings->select('rating')->get() as $rating){
                return $sum = $sum + $rating->rating;
            }
            $sum = $sum/$ratings->count();
            return $sum;
        }
        // return 0;
    }
}
