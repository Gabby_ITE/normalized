<?php

namespace App\Http\Resources\Book;

use App\Models\Book\Book;
use App\Models\Book\BookRating;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BookReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'book_id'=>$this->book_id,
            'user_name'=>$this->user->first_name .
                            ' ' . $this->user->last_name,
            'image'=>$this->user->image,
            'rating'=>$this->rate(),
            'text'=>$this->text,
            'is_burn'=>$this->is_burn,
            'your_like'=>$this->your_like($request),
            'likes'=>$this->likes()->count(),
            'your_dis_like'=>$this->your_dis_like($request),
            'dis_likes'=>$this->dis_likes()->count(),
            'created_at'=>Carbon::parse($this->created_at)->diffForHumans(),
        ];
    }

    private function your_like(Request $request)
    {
        return $this->likes()->where('user_id',$request->user()->id)->exists();
    }
    private function your_dis_like(Request $request)
    {
        return $this->dis_likes()->where('user_id',$request->user()->id)->exists();
    }
    private function rate()
    {
        $book = Book::find($this->book_id);
        $rate = $book->ratings()->select('rating')
            ->where('user_id',$this->user->id)
            ->get();

        if ($rate->count())
            return $rate[0]->rating;
        return null;
    }
}
