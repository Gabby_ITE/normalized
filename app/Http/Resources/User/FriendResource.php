<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FriendResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = null;
        if ($this->sender_id == $request->user()->id){
            $user = $this->receiver;
        }else if ($this->receiver_id == $request->user()->id){
            $user = $this->sender;
        }
        return [
            'id'=>$this->id,
            'user'=>new ManyUserResource($user)
        ];
    }
}
