<?php

namespace App\Http\Controllers\Api\Club;

use App\Http\Controllers\Controller;
use App\Http\Requests\Club\StoreClubRequest;
use App\Http\Requests\Club\UpdateClubRequest;
use App\Http\Resources\Club\ManyClubResource;
use App\Http\Resources\Club\SingleClubResource;
use App\Models\Club\Club;
use App\Models\Club\ClubMember;
use App\Models\Tag\ClubTag;
use App\Models\Tag\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (auth()->user()->role == 'admin'){
            return ManyClubResource::collection(Club::all());
        }
        return ManyClubResource::collection(Club::where('is_private',false)->get());
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreClubRequest $request)
    {
        $request->validated();

        $is_closed = $request->is_closed;
        if ($request->private){
            $is_closed = $request->is_private;
        }

        $image = time() . '-' . $request->name . '.' . $request->image->extension();
        $request->image->move(public_path('clubs/club_images'),$image);
        $club = Club::create([
            'admin_id'=>$request->user()->id,
            'name'=>$request->name,
            'is_private'=>$request->is_private,
            'is_closed'=>$is_closed,
            'description'=>$request->description,
            'image'=>'clubs/club_images/'.$image,
        ]);
        ClubMember::create([
            'user_id'=>$request->user()->id,
            'club_id'=>$club->id
        ]);
        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = Tag::all()->find($tagId);
                $exits = false;
                if ($tag){
                    foreach ($club->tags()->get() as $clubTag){
                        if ($clubTag->tag->id == $tag->id){
                            $exits = true;
                            break;
                        }
                    }
                    if(!$exits){
                        ClubTag::create([
                            'tag_id'=> $tag->id,
                            'club_id'=>$club->id,
                        ]);
                    }
                }
            }
        }
        return new SingleClubResource($club);
    }

    public function updateImage(Request $request,Club $club){
        $request->validate(['image'=>'file|image']);
        File::delete(public_path($club->image));
        $image = time() . '-' . $club->name . '.' . $request->file('image')->extension();
        $request->image->move(public_path('clubs/club_images'),$image);
        $club->update([
            'image'=>'clubs/club_images/'.$image,
        ]);
        return new SingleClubResource($club);
    }

    /**
     * Display the specified resource.
     */
    public function show(Club $club)
    {
        if ($club->is_private){
            if (auth()->user()->role == 'admin' || $club->admin_id == auth()->user()->id)
                return $club;
        }
        else
            return new SingleClubResource($club);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateClubRequest $request, Club $club)
    {
        if ($club->admin_id != auth()->user()->id){
            return response()->json(['message'=>'you are not allowed'],403);
        }
        $club->update($request->validated());
        if ($request->image != null){
            $image = time() . '-' . $request->name . '.' . $request->image->extension();
            $request->image->move(public_path('clubs/club_images'),$image);
            // Storage::delete($club->image);

            $club->update([
                'image'=>'clubs/club_images/'.$image,
            ]);
        }
        if ($club->is_private){
            $club->update([
                'is_closed'=>$club->is_private
            ]);
        }
        return new SingleClubResource($club);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Club $club)
    {
        if ($club->admin_id == auth()->user()->id || auth()->user()->role == 'admin'){
            Storage::delete($club->image);
            $club->delete();
            return response()->noContent();
        }
        return response()->json(['message'=>'you are not allowed'],403);
    }
}
