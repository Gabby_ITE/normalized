<?php

namespace App\Http\Controllers\Api\Club;

use App\Http\Controllers\Controller;
use App\Http\Resources\Club\ManyClubResource;
use App\Http\Resources\User\ManyUserResource;
use App\Models\Club\Club;
use Illuminate\Http\Request;

class ClubMemberController extends Controller
{
    public function index(Club $club)
    {
        return ManyUserResource::collection($club->members()->get()->pluck('user'));
    }
    public function joined_clubs()
    {
        return ManyClubResource::collection(auth()->user()->joined_clubs->pluck('club'));
    }
    public function friends_joined_clubs()
    {
        $friends = auth()->user()->friends;
        $friendData = [];
        $friendCounter = 0;
        foreach($friends as $friend){
            if ($friend->sender_id == auth()->user()->id){
                $friendData[$friendCounter++]= $friend->receiver;
            }else{
                $friendData[$friendCounter++]= $friend->sender;
            }
        }
        $data = [];
        $counter = 0;
        foreach($friendData as $friend){
            $data[$counter++] = [new ManyUserResource($friend), ManyClubResource::collection($friend->joined_clubs->pluck('club'))];
        }

        return $data;
    }
    public function add(Request $request,Club $club)
    {
        if (auth()->user()->id != $club->admin_id){
            return response()->json(['message'=>'you are not authorized'],403);
        }
        $request->validate([
            'user_id'=>['required','exists:users,id']
        ]);

        if ($club->members()->where('user_id',$request->user_id)->exists() == 1){
            return response()->json(['message'=>'its been added'],403);
        }

        $club->members()->create([
            'user_id'=>$request->user_id,
        ]);
        return ManyUserResource::collection($club->members()->get()->pluck('user'));
    }
    public function delete(Club $club, string $userId)
    {
        if (auth()->user()->id != $club->admin_id){
            return response()->json(['message'=>'you are not authorized'],403);
        }
        if ($club->members()->where('user_id',$userId)->exists()==1){
            $club->members()->where('user_id',$userId)->get()->first()->delete();
            return response()->noContent();
        }
        return response()->json(['message'=>'not found'],404);
    }
}
