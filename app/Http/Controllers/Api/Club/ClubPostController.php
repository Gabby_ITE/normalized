<?php

namespace App\Http\Controllers\Api\Club;

use App\Http\Controllers\Controller;
use App\Http\Resources\Club\ManyClubPostResource;
use App\Http\Resources\Club\SingleClubPostResource;
use App\Models\Club\Club;
use App\Models\Club\ClubPost;
use Illuminate\Http\Request;

class ClubPostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Club $club)
    {
        if ($club->is_closed || $club->is_private){
            if ($club->members->where('user_id',auth()->user()->id)){
                return ManyClubPostResource::collection($club->posts()->get()->sortByDesc('created_at'));
            }else{
                return response()->json(['message'=>'you can\'t do that' ],403);
            }
        }
        return ManyClubPostResource::collection($club->posts()->get()->sortByDesc('created_at'));

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Club $club)
    {
        if ($club->members()->where('user_id',auth()->user()->id)->exists() == 0)
            return response()->json(['message'=>'you can\'t do that' ],403);
        $request->validate([
            'text'=>'required'
        ]);
        $post = ClubPost::create([
            'user_id'=>$request->user()->id,
            'club_id'=>$club->id,
            'text'=>$request->text,
        ]);
        return new SingleClubPostResource($post);
    }

    /**
     * Display the specified resource.
     */
    public function show(Club $club,string $post)
    {
        $post = ClubPost::find($post);
        if ($post == null){
            return response()->json(['message'=>'not found'],404);
        }
        if ($post->club_id == $club->id){
            return new SingleClubPostResource($post);
        }
        return response()->json(['message'=>'not in this club'],403);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,Club $club, string $post)
    {
        $request->validate([
            'text'=>'required'
        ]);
        $post = ClubPost::find($post);
        if ($post == null){
            return response()->json(['message'=>'not found'],404);
        }
        if (auth()->user()->id != $club->admin_id || auth()->user()->id != $post->user_id){
            return response()->json(['message'=>'not autherized'],403);
        }
        if ($post->club_id == $club->id){
            $post->update(['text'=>$request->text]);
            return new SingleClubPostResource($post);
        }
        return response()->json(['message'=>'not in this club'],403);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Club $club, string $post)
    {
        $post = ClubPost::find($post);
        if ($post == null){
            return response()->json(['message'=>'not found'],404);
        }
        if (auth()->user()->id != $club->admin_id || auth()->user()->id != $post->user_id){
            return response()->json(['message'=>'not autherized'],403);
        }
        if ($post->club_id == $club->id){
            $post->delete();
            return response()->noContent();
        }
        return response()->json(['message'=>'not in this club'],403);
    }
}
