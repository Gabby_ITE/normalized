<?php

namespace App\Http\Controllers\Api\Club;

use App\Http\Controllers\Controller;
use App\Http\Resources\Club\SingleClubPostResource;
use App\Models\Club\Club;
use App\Models\Club\ClubPost;
use App\Models\Club\ClubPostReply;
use Illuminate\Http\Request;

class ClubPostReplyController extends Controller
{
    public function store(Request $request,Club $club,string $post)
    {
        if ($club->members()->where('user_id',auth()->user()->id)->exists() == 0)
            return response()->json(['message'=>'you can\'t do that' ],403);
        $request->validate(['text'=>'required']);
        $post = ClubPost::find($post);
        if ($post == null){
            return response()->json(['message'=>'not found'],404);
        }
        if ($post->club_id == $club->id){
            ClubPostReply::create([
                'user_id'=>$request->user()->id,
                'club_post_id'=>$post->id,
                'text'=>$request->text,
            ]);
            return new SingleClubPostResource($post);
        }else{
            return response()->json(['message'=>'this post not in this club'],403);
        }
    }
    public function store_to_reply(Request $request,Club $club,string $post,string $reply)
    {
        if ($club->members()->where('user_id',auth()->user()->id)->exists() == 0)
            return response()->json(['message'=>'you can\'t do that' ],403);
        $request->validate(['text'=>'required']);
        $post = ClubPost::find($post);
        if ($post == null){
            return response()->json(['message'=>'not found'],404);
        }
        if ($post->club_id == $club->id){
            $reply = ClubPostReply::find($reply);
            ClubPostReply::create([
                'user_id'=>$request->user()->id,
                'club_post_id'=>$post->id,
                'club_post_reply_id'=>$reply->id,
                'text'=>$request->text,
            ]);
            return new SingleClubPostResource($post);
        }else{
            return response()->json(['message'=>'this post not in this club'],403);
        }
    }
    public function update(Request $request,Club $club,string $post,string $reply){
        $request->validate(['text'=>'required']);
        $reply = ClubPostReply::find($reply);
        if ($reply == null){
            return response()->json(['message'=>'not found'],404);
        }
        $post = ClubPost::find($post);
        if ($post == null){
            return response()->json(['message'=>'not found'],404);
        }
        if (auth()->user()->id != $club->admin_id || auth()->user()->id != $reply->user_id){
            return response()->json(['message'=>'not autherized'],403);
        }
        if ($post->club_id == $club->id){
            $reply->update(['text'=>$request->text]);
        }else{
            return response()->json(['message'=>'this post not in this club'],403);
        }
        return new SingleClubPostResource($reply);
    }
    public function destroy(Club $club,string $post,string $reply)
    {
        $reply = ClubPostReply::find($reply);
        if ($reply == null){
            return response()->json(['message'=>'not found'],404);
        }
        $post = ClubPost::find($post);
        if ($post == null){
            return response()->json(['message'=>'not found'],404);
        }
        if (auth()->user()->id != $club->admin_id || auth()->user()->id != $reply->user_id){
            return response()->json(['message'=>'not autherized'],403);
        }
        if ($post->club_id == $club->id){
            $reply->delete();
            return response()->noContent();
        }else{
            return response()->json(['message'=>'this post not in this club'],403);
        }
    }
    public function show(Club $club,string $post,string $reply)
    {
        $reply = ClubPostReply::find($reply);
        if ($reply == null){
            return response()->json(['message'=>'not found'],404);
        }
        $post = ClubPost::find($post);
        if ($post == null){
            return response()->json(['message'=>'not found'],404);
        }
        if ($post->club_id == $club->id){
            return new SingleClubPostResource($reply);
        }else{
            return response()->json(['message'=>'this post not in this club'],403);
        }
    }
}
