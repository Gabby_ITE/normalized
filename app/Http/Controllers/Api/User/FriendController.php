<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\FriendResource;
use App\Http\Resources\User\ManyUserResource;
use App\Models\Friend;
use App\Models\User;
use Illuminate\Http\Request;

class FriendController extends Controller
{
    public function index()
    {
        return FriendResource::collection(auth()->user()->friends);
    }

    public function store(Request $request)
    {
        $request->validate(['user_id'=>['required','exists:users,id']]);
        if ($request->user()->friends()->where('sender_id',$request->user_id)->exists() == 1
            || $request->user()->friends()->where('receiver_id',$request->user_id)->exists() == 1){
                return response()->json([
                    'message'=>'you are friends with ' . User::find($request->user_id)->first_name
                ],403);
            }
        $friend = Friend::create([
            'sender_id'=>$request->user()->id,
            'receiver_id'=>$request->user_id
        ]);
        return new FriendResource($friend);
    }

    public function destroy(Friend $friend)
    {
        if (auth()->user()->id == $friend->sender_id || auth()->user()->id == $friend->receiver_id){
            $friend->delete();
            return response()->noContent();
        }
        return response()->json(['message'=>'you can\'t do that'],403);
    }
}
