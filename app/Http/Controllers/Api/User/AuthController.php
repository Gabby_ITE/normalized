<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Requests\User\ValidationCodeRequest;
use App\Mail\VerifyEmail;
use App\Models\Logout;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rules\Password;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $request->validated();
        $validationCode = rand(100000,999999);
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'validation_code' => $validationCode,
        ]);
        if ($request->role){
            $user->role = $request->role;
        }
        if ($request->image){
            $user->image= $request->file('image')->store('users/cover_images');
        }
        $user->save();
        try{
            Mail::to($user->email)->send(new VerifyEmail($user));

        }catch(Exception $excep){
            return response()->json([$excep->getMessage()],401) ;
        }
        return response()->json(['message' => 'User registered successfully.'],201);

    }
    public function validateRegistration(ValidationCodeRequest $request)
    {
        $request->validated();
        $user = User::where('email', $request->email)
        ->where('validation_code', $request->validation_code)
        ->first();

        if ($user) {
            $user->email_verified_at = now();
            $user->save();
            return response()->json([
                'message' => 'Registration complete.',
                'token' => $user->createToken('Api Token')->plainTextToken,
            ], 200);
        } else {
            // validation code does not match, show an error message
            return response()->json(['message' => 'Invalid validation code.'], 422);
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'email'=>['required','email'],
            'password'=>['required']
        ]);

        if (! auth()->attempt($request->only(['email','password']))){
            return response()->json([
                'message'=>'Credentials do not match'
            ],401);
        }
        $user = User::where('email',$request->email)->first();
        return response()->json([
            'data'=> [
                'user'=>$user,
                'token'=>$user->createToken('Api Token')->plainTextToken,
            ]
        ],200);
    }

    public function logout()
    {
        auth()->user()->currentAccessToken()->delete();
        Logout::create(['user_id'=>auth()->user()->id]);
        return response()->noContent();
    }

    public function resetPassword()
    {
        $validationCode = rand(100000,999999);
        auth()->user()->update([
            'validation_code'=>$validationCode
        ]);
        Mail::to(auth()->user()->email)->send(new VerifyEmail(auth()->user()));
        return response()->json(['message' => ' "Validation Code" has been send'],201);
    }

    public function validateResetPassword(ValidationCodeRequest $request)
    {
        $request->validated();
        $request->validate([
            'password'=>['required','confirmed',Password::defaults()]
        ]);

        $user = User::where('email', $request->email)
        ->where('validation_code', $request->validation_code)
        ->first();

        if ($user) {
            $user->email_verified_at = now();
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json([
                'message' => 'you change your password.',
            ], 200);
        }
        else {
            return response()->json(['message' => 'Invalid validation code.'], 422);
        }
    }
}
