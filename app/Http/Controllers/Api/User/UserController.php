<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\User\ManyUserResource;
use App\Http\Resources\User\SingleUserResource;
use App\Models\User;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request as Req;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json([
            'data'=>ManyUserResource::collection(User::select('id','first_name','last_name','image')),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RegisterRequest $request)
    {
        $this->middleware(['admin']);
        $request->validated();
        $validationCode = rand(100000,999999);

        $user = User::create([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'validation_code' => $validationCode,
        ]);

        if ($request->role){
            $user->role = $request->role;
        }
        if ($request->image){
            $image = time() . '-' . $request->name . '.' . $request->image->extension();
            $request->image->move(public_path('users/cover_images'),$image);
            $user->image = 'users/cover_images/'.$image;
        }
        $user->save();

        return new SingleUserResource($user);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        return new SingleUserResource($user);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        if (auth()->user() != $user ){
            $this->middleware(['admin']);
        }
        $user->update($request->validated());
        if($request->image != null){
            $imagePath = $user->image;
            if (File::exists($user->image)){
                File::delete(public_path($imagePath));
            }
            $image = time() . '-' . $request->name . '.' . $request->image->extension();
            $request->image->move(public_path('users/cover_images'),$image);
            $user->image = 'users/cover_images/'.$image;
            $user->save();
        }


        if (auth()->user()->role == 'admin'){
            if ($request->role == 'user' || $request->role == 'admin'){
                $user->update([
                    'role'=>$request->role
                ]);
            }
        }
        return new SingleUserResource($user);
    }

    public function updateImage(Req $request,User $user){
        $request->validate(['image'=>'file|image']);
        if (File::exists($user->image)){
            File::delete(public_path($user->image));
        }
        $image = time() . '-' . $user->name . '.' . $request->file('image')->extension();
        $request->image->move(public_path('users/cover_images'),$image);
        $user->update([
            'image'=>'users/cover_images/'.$image,
        ]);
        return new SingleUserResource($user);
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        if (auth()->user() != $user ){
            $this->middleware(['admin']);
        }

        $imagePath = $user->image;
        Storage::delete($imagePath);

        $user->delete();
        return response()->noContent();
    }
}
