<?php

namespace App\Http\Controllers\Api\Search;

use App\Http\Controllers\Controller;
use App\Http\Resources\Author\ManyAuthorResource;
use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\TagResource;
use App\Models\Book\Book;
use App\Models\Tag\Tag;
use Illuminate\Http\Request;

class ForYouController extends Controller
{
    public function booksByTags(Tag $tag)
    {
        $authors = auth()->user()->authors;
        $books = $tag->books->pluck('book')->sortByDesc('release_date');
        $favoriteBooks = [];
        $nextBooks = [];
        $fCounter = 0;
        $nCounter = 0;

        // return ManyBookResource::collection($books);
        foreach($books as $book){
            $favorite = false;
            foreach($book->authors as $bauthor){
                foreach($authors as $author){
                    if ($author->author_id == $bauthor->author_id){
                        $favorite = true;
                        $favoriteBooks[$fCounter++] = $book;
                        break;
                    }
                }
                if ($favorite){
                    break;
                }
            }
            if (!$favorite){
                $nextBooks[$nCounter++] = $book;
            }
        }
        $counter = 0;
        foreach ($nextBooks as $book){
            if ($counter < $nCounter){
                $favoriteBooks[$fCounter++] = $book;
                $counter++;
            }
        }
        $favoriteBooks = collect($favoriteBooks);
        return ManyBookResource::collection($favoriteBooks);
    }
}
