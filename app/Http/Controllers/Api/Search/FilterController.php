<?php

namespace App\Http\Controllers\Api\Search;

use App\Http\Controllers\Controller;
use App\Http\Resources\TagResource;
use App\Models\Book\Book;
use App\Models\Logout;
use App\Models\Request\Report;
use App\Models\Tag\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;


class FilterController extends Controller
{
    public function top_5_tags_most_rated_in_past_12_months()
    {
    $today = Carbon::now();
    $tags = Tag::all();
    $tags_books = [];
    $counter1 = 0;

    foreach($tags as $tag){
        $tags_books[$counter1++] = ['tag'=>$tag,'books'=>$tag->books()->get()->pluck('book')];
    }
    $fullResult = collect();
    $counter1 = 0;
    $month = $today->month;
    $year = $today->year;
    $thisYear = $today->year;
    for ($i = 0; $i < 12; $i++) {
        if ($thisYear != $today->year){
            break;
        }
        $counter3 = 0;
        $result = [];
        foreach($tags_books as $tag_book){
            $books_ratings = [];
            $counter2 = 0;
            foreach($tag_book['books'] as $book){
                if ($book->ratings->isEmpty()){
                    continue;
                }
                $rating = $book->ratings()->where('updated_at','>=',"$year-$month-01")
                    ->where('updated_at','<=',"$year-$month-31")->get();
                if ($rating->count() == 0){
                    continue;
                }
                $sum = 0;
                foreach($rating as $rate){
                    $sum += $rate->rating;
                }
                $sum = $sum/$rating->count();
                $books_ratings[$counter2++] =$sum;
            }
            if ($counter2 > 0){
                $sum = 0;
                foreach($books_ratings as $rate){
                    $sum += $rate;
                }
                $sum = $sum/$counter2;
                $result[$counter3++] = ['tag'=>$tag_book['tag'],'rate'=>$sum];

            }
        }
        $result = collect($result);
        $result = $result->sortByDesc('rate');
        $newResult = collect();
        $c = 0;
        foreach($result as $r){
            if ($c == 5)
                break;
            $newResult->add($r);
            $c ++ ;
        }
        $fullResult->add([$month,$year,$newResult]);
        $month = $today->subMonth()->month;

        $year = $today->year;
    }
    return response()->json(['data'=>$fullResult]);
    }

    public function number_of_new_users_each_week_in_month()
    {
        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;
        $firtWeek = User::where('created_at','>=',"$year-$month-01")
            ->where('created_at','<=',"$year-$month-07")->count();
        $secondWeek = User::where('created_at','>=',"$year-$month-08")
            ->where('created_at','<=',"$year-$month-14")->count();
        $thirdWeek = User::where('created_at','>=',"$year-$month-15")
            ->where('created_at','<=',"$year-$month-21")->count();
        $fourthdWeek = User::where('created_at','>=',"$year-$month-22")
            ->where('created_at','<=',"$year-$month-31")->count();
        return response()->json(['data'=>[$firtWeek,$secondWeek,$thirdWeek,$fourthdWeek]]);
    }

    public function number_of_logout_users_each_week_in_month()
    {
        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;
        $users = User::all();
        $firtWeek = 0;
        $secondWeek = 0;
        $thirdWeek = 0;
        $fourthdWeek = 0;
        foreach($users as $user){
            $firtWeek += $user->logouts->where("created_at",">=","$year-$month-01")
            ->where("created_at","<","$year-$month-08")->count();
        }

        foreach($users as $user){
            $secondWeek += $user->logouts->here("created_at",">","$year-$month-07")
            ->where("created_at","<","$year-$month-15")->count();
        }


        foreach($users as $user){
            return $user->logouts->where("created_at",">","$year-$month-14")
            ->where("created_at","<","$year-$month-22")->count();
        }

        foreach($users as $user){
            $fourthdWeek += $user->logouts->where("created_at",">","$year-$month-21")
            ->where("created_at","<","$year-$month-32")->count();
        }
        return response()->json(["data"=>[$firtWeek,$secondWeek,$thirdWeek,$fourthdWeek]]);
    }


    public function number_of_reports_each_week_in_month()
    {
        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;
        $reports = Report::all();
        $firtWeek = 0;
        $secondWeek = 0;
        $thirdWeek = 0;
        $fourthdWeek = 0;

        $firtWeek = Report::where("created_at",">=","$year-$month-01")
            ->where("created_at","<=","$year-$month-07")->count();

        $secondWeek = Report::where("created_at",">=","$year-$month-08")
            ->where("created_at","<=","$year-$month-14")->count();


        $thirdWeek = Report::where("created_at",">","$year-$month-14")
            ->where("created_at","<=","$year-$month-21")->count();

        $fourthdWeek = Report::where("created_at",">=","$year-$month-22")
            ->where("created_at","<=","$year-$month-31")->count();
        return response()->json(["data"=>[$firtWeek,$secondWeek,$thirdWeek,$fourthdWeek]]);
    }

    public function number_of_users_who_rate_book_each_week_in_month()
    {
        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;
        $users = User::all();
        $firtWeek = 0;
        $secondWeek = 0;
        $thirdWeek = 0;
        $fourthdWeek = 0;
        foreach($users as $user){
            $firtWeek += $user->ratings()->where('updated_at','>=',"$year-$month-01")
            ->where('updated_at','<=',"$year-$month-07")->exists();
        }

        foreach($users as $user){
            $secondWeek += $user->ratings()->where('updated_at','>=',"$year-$month-08")
            ->where('updated_at','<=',"$year-$month-14")->exists();
        }

        foreach($users as $user){
            $thirdWeek += $user->ratings()->where('updated_at','>=',"$year-$month-15")
            ->where('updated_at','<=',"$year-$month-21")->exists();
        }

        foreach($users as $user){
            $fourthdWeek += $user->ratings()->where('updated_at','>=',"$year-$month-22")
            ->where('updated_at','<=',"$year-$month-31")->exists();
        }
        return response()->json(['data'=>[$firtWeek,$secondWeek,$thirdWeek,$fourthdWeek]]);
    }

    public function number_of_users_who_completed_challenges_each_week_in_month()
    {
        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;
        $users = User::all();
        $firtWeek = 0;
        $secondWeek = 0;
        $thirdWeek = 0;
        $fourthdWeek = 0;
        foreach($users as $user){
            $firtWeek += $user->progressed_challenges()
            ->where('created_at','>=',"$year-$month-01")
            ->where('created_at','<=',"$year-$month-07")
            ->where('status','finished')->exists();
        }

        foreach($users as $user){
            $secondWeek += $user->progressed_challenges()
            ->where('created_at','>=',"$year-$month-08")
            ->where('created_at','<=',"$year-$month-14")
            ->where('status','finished')->exists();
        }

        foreach($users as $user){
            $thirdWeek += $user->progressed_challenges()
            ->where('created_at','>=',"$year-$month-15")
            ->where('created_at','<=',"$year-$month-21")
            ->where('status','finished')->exists();
        }

        foreach($users as $user){
            $fourthdWeek += $user->progressed_challenges()
            ->where('created_at','>=',"$year-$month-22")
            ->where('created_at','<=',"$year-$month-31")
            ->where('status','finished')->exists();
        }
        return response()->json(['data'=>[$firtWeek,$secondWeek,$thirdWeek,$fourthdWeek]]);
    }

    public function percentage_tags(){
        $tags = Tag::all();
        $books = Book::all();
        $books_number = $books->count();
        $result = collect();
        foreach($tags as $tag){
            $number = $tag->books()->count();
            $sum = ($number*100)/$books_number;
            $result->add(['tag'=>new TagResource($tag),'percentage'=>$sum]);
        }
        return response()->json(['data'=>$result->sortByDesc('percentage')]);
    }
}
