<?php

namespace App\Http\Controllers\Api\Search;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\User\ManyUserResource;
use App\Models\Friend;
use Illuminate\Http\Request;

class FriendFeedsController extends Controller
{
    public function friends_suggests()
    {
        $friends = auth()->user()->friends;
        $friends_friends = collect();
        foreach($friends as $friend){
            foreach($friend->user()->friends as $newFriend){
                if ($this->not_in_friends($newFriend)){
                    $mutual_friends = $this->mutual_friends($newFriend);
                    $friends_friends->add([
                        'user'=>new ManyUserResource($newFriend->user()),
                        'mutual_friends'=>ManyUserResource::collection($mutual_friends),
                        'count'=>$mutual_friends->count()
                    ]);
                }
            }
        }
        return response()->json(['data'=>$friends_friends->sortByDesc('count')]);
    }
    private function not_in_friends(Friend $newFriend){
        if ($newFriend->user() == auth()->user()){
            return false;
        }
        foreach(auth()->user()->friends as $friend){
            if ($friend->user() == $newFriend->user()){
                return true;
            }
        }
        return false;
    }
    private function mutual_friends(Friend $newFriend){
        $newFriendList = $newFriend->user()->friends;
        $mutual_friends = collect();
        foreach($newFriendList as $friend){
            if ($this->not_in_friends($friend)){
                continue;
            }
            $mutual_friends->add($friend->user());
        }
        return $mutual_friends;
    }
    public function books()
    {
        $friends = auth()->user()->friends->pluck('user');
        $books = [];
        $counter = 0;
        foreach($friends as $friend){
            foreach ($friend->book_statuses as $status){
                $books[$counter++] = $status->book;
            }
        }

        $books = collect($books);
        return ManyBookResource::collection($books->unique('id',true)->sortByDesc('release_date'));

    }

    public function book_statuses()
    {
        $friends = auth()->user()->friends->pluck('user');
        $statuses = [];
        $counter = 0;
        foreach($friends as $friend){
            foreach ($friend->book_statuses as $status){
                $statuses[$counter++] = $status;
            }
        }

        $statuses = collect($statuses);
        return $statuses->sortByDesc('updated_at');
    }

    public function challenges()
    {
        $friends = auth()->user()->friends->pluck('user');
        $progresses = [];
        $counter = 0;
        foreach($friends as $friend){
            foreach ($friend->progressed_challenges as $progress){
                $progresses[$counter++] = $progress;
            }
        }

        $progresses = collect($progresses);
        return $progresses->sortByDesc('updated_at');
    }
}
