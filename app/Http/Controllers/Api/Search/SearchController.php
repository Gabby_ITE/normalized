<?php

namespace App\Http\Controllers\Api\Search;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\ManyBookResource;
use App\Models\Book\Book;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searchByTags(Request $request)
    {
        $request->validate(['tags'=>['required','array','exists:users,id']]);
        $books = Book::all()->sortByDesc('release_date');
        $data = [];
        $counter = 0;
        foreach ($books as $book){
            $has = true;
            foreach($request->tags as $tag){
                if ($book->tags()->where('tag_id',$tag)->exists() == 0){
                    $has = false;
                }
                if ($has){
                    $data[$counter] = $book;
                    $counter++;
                }
            }
        }
        return ManyBookResource::collection($data);
    }

    public function forMe()
    {
        $books = Book::all()->sortByDesc('release_date');
        $dataTag = [];
        $counterTag = 0;
        $dataAuthor = [];
        $counterAuthor = 0;
        foreach ($books as $book){
            foreach(auth()->user()->tags as $tag){
                if ($book->tags()->where('tag_id',$tag->tag_id)->exists() == 1){
                    $dataTag[$counterTag] = $book;
                    $counterTag++;
                    break;
                }
            }
        }
        foreach ($books as $book){
            foreach(auth()->user()->authors as $author){
                if ($book->authors()->where('author_id',$author->author_id)->exists() == 1){
                    $dataAuthor[$counterAuthor] = $book;
                    $counterAuthor++;
                    break;
                }
            }
        }
        $dataAuthor = collect($dataAuthor);
        $dataTag = collect($dataTag);
        $data = $dataAuthor->union($dataTag)->sortByDesc('release_date');
        return ManyBookResource::collection($data);
    }
}
