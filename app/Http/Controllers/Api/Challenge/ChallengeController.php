<?php

namespace App\Http\Controllers\Api\Challenge;

use App\Http\Controllers\Controller;
use App\Http\Requests\Challenge\StoreChallengeRequest;
use App\Http\Requests\Challenge\UpdateChallengeRequest;
use App\Http\Resources\Challenge\ManyChallengeResource;
use App\Http\Resources\Challenge\SingleChallengeResource;
use App\Models\Challenge\Challenge;
use Illuminate\Http\Request;

class ChallengeController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function index()
    {
        if(auth()->user()->role == 'admin'){
            return ManyChallengeResource::collection(Challenge::get()->sortByDesc('updated_at'));
        }
        return ManyChallengeResource::collection(
            Challenge::where('is_private',false)->get()->sortByDesc('updated_at')
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreChallengeRequest $request)
    {
        $request->validate([
            'book_id'=>['exists:books,id']
        ]);

        $request->validated();
        if ($request->start_date > $request->end_date){
            return response()->json([
                'message'=>'you can not do that'
            ]);
        }
        $is_private = 1;
        if ($request->is_private == 0 || $request->is_private == 1){
            $is_private = $request->is_private;
        }
        $book_id = null;
        if ($request->book_id){
            $book_id = $request->book_id;
        }
        $challenge = Challenge::create([
            'creator_id'=>$request->user()->id,
            'book_id'=>$book_id,
            'name'=>$request->name,
            'is_private'=>$is_private,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'type'=>$request->type,
            'max_number'=>$request->max_number,
        ]);
        return new SingleChallengeResource($challenge);
    }

    /**
     * Display the specified resource.
     */
    public function show(Challenge $challenge)
    {
        if ($challenge->is_private == 1){
            if ($challenge->creator_id == auth()->user()->id || auth()->user()->role == 'admin')
                return new SingleChallengeResource($challenge);
            else{
                return response()->isInvalid();
            }
        }else{
            return new SingleChallengeResource($challenge);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateChallengeRequest $request, Challenge $challenge)
    {
        if ($challenge->creator_id != auth()->user()->id){
            return response()->json(['message'=>'not authorized'],403);
        }
        $challenge->update($request->validated());
        return new SingleChallengeResource($challenge);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Challenge $challenge)
    {
        if ($challenge->creator_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'not authorized'],403);
        }
        $challenge->deleteOrFail();
        return response()->noContent();
    }
}
