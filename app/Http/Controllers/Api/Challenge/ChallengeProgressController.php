<?php

namespace App\Http\Controllers\Api\Challenge;

use App\Http\Controllers\Controller;
use App\Http\Resources\Challenge\ChallengeProgressResource;
use App\Models\Challenge\Challenge;
use App\Models\Challenge\ChallengeProgress;
use Illuminate\Http\Request;

class ChallengeProgressController extends Controller
{
    public function index(Challenge $challenge)
    {
        if ($challenge->is_private == 1){
            if ($challenge->creator_id == auth()->user()->id)
                return ChallengeProgressResource::collection($challenge->progresses()->get());
            else{
                return response()->isInvalid();
            }
        }else{
            return ChallengeProgressResource::collection($challenge->progresses()->get());
        }
    }
    public function store(Request $request,Challenge $challenge)
    {
        if ($challenge->progresses()->where('user_id',$request->user()->id)->exists() == 1){
            return response()->json([
                'message'=>'you aleady add this challenge'
            ],403);
        }
        $challengeProgress = ChallengeProgress::create([
            'challenge_id'=>$challenge->id,
            'user_id'=>$request->user()->id,
            'acheived_number'=>0
        ]);
        return new ChallengeProgressResource($challengeProgress);
    }

    public function update(Request $request,Challenge $challenge)
    {
        $request->validate([
            'acheived_number' => 'required'
        ]);
        if ($request->acheived_number > $challenge->max_number){
            return response()->json(['message'=>'you can not do that'],403);
        }
        $status = $this->getStatus($challenge,$request->acheived_number);
        foreach ($challenge->progresses()->where('user_id',auth()->user()->id)->get() as $progress){
            $progress->update([
                'acheived_number'=> $request->acheived_number,
                'status'=> urlencode($status),
            ]);
        }
        return new ChallengeProgressResource(
            $challenge->progresses()->where('user_id',auth()->user()->id)->get()->first()
        );
    }
    private function getStatus(Challenge $challenge,$acheiRequest $request, ved_number){
        if (now() < $challenge->end_date){
            if ($acheived_number < $challenge->max_number)
                return 'not_decide';
            else
                return 'finished';
        }
        return 'timeout';
    }
    public function delete(Challenge $challenge){
        if ($challenge->progresses()->where('user_id',auth()->user()->id)->exists() == 1){
            $challenge->progresses()->where('user_id',auth()->user()->id)->delete();
            return response()->noContent();
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }
}
