<?php

namespace App\Http\Controllers\Api\Challenge;

use App\Http\Controllers\Controller;
use App\Http\Resources\Challenge\ChallengeProgressResource;
use App\Http\Resources\Challenge\ManyChallengeResource;
use App\Models\Book\Book;
use Illuminate\Http\Request;

class UserChallengeController extends Controller
{
    public function created_challenges(){
        return ManyChallengeResource::collection(auth()->user()->created_challenges->sortByDesc('updated_at'));
    }
    public function progressed_challenges($status)
    {
        return ChallengeProgressResource::collection(
            auth()->user()->progressed_challenges->where('status',$status)->sortByDesc('updated_at')
        );
    }
    public function chall(Request $request,Book $book){

    }
}
