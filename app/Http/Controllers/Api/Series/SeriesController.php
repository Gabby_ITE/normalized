<?php

namespace App\Http\Controllers\Api\Series;

use App\Http\Controllers\Controller;
use App\Http\Requests\Series\StoreSeriesRequest;
use App\Http\Requests\Series\UpdateSeriesRequest;
use App\Http\Resources\Series\ManySeriesResource;
use App\Http\Resources\Series\SingleSeriesResource;
use App\Models\Author\Author;
use App\Models\Author\AuthorSeries;
use App\Models\Book\Book;
use App\Models\Series\BookSeries;
use App\Models\Series\Series;
use App\Models\Series\SeriesDescription;
use App\Models\Tag\SeriesTag;
use App\Models\Tag\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SeriesController extends Controller
{
    public function index()
    {
        return ManySeriesResource::collection(Series::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSeriesRequest $request)
    {
        $this->middleware(['admin']);
        $request->validated();
        $image = time() . '-' . $request->title . '.' . $request->file('cover')->extension();
        $request->cover->move(public_path('series/cover_images'),$image);
        $cover='series/cover_images/'.$image;
        $series = Series::create([
            'title'=>$request->title,
            'cover'=>$cover,
            'release_date'=>$request->release_date,
        ]);
        if ($request->authors){
            foreach($request->authors as $authorId){
                $author = Author::all()->find($authorId);
                $exits = false;
                if ($author){
                    foreach ($series->authors()->get() as $authorSeries){
                        if ($authorSeries->author->id == $author->id){
                            $exits = true;
                            break;
                        }
                    }
                    if(!$exits){
                        AuthorSeries::create([
                            'author_id'=> $author->id,
                            'series_id'=>$series->id,
                        ]);
                    }
                }
            }
        }

        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = Tag::all()->find($tagId);
                $exits = false;
                if ($tag){
                    foreach ($series->tags()->get() as $seriesTag){
                        if ($seriesTag->tag->id == $tag->id){
                            $exits = true;
                            break;
                        }
                    }
                    if(!$exits){
                        SeriesTag::create([
                            'tag_id'=> $tag->id,
                            'series_id'=>$series->id,
                        ]);
                    }
                }
            }
        }
        if ($request->books){
            foreach($request->books as $bookId){
                $book = Book::all()->find($bookId);
                $exits = false;
                if ($book){
                    foreach ($series->books()->get() as $seriesBook){
                        if ($seriesBook->book->id == $book->id){
                            $exits = true;
                            break;
                        }
                    }
                    if(!$exits){
                        BookSeries::create([
                            'book_id'=> $book->id,
                            'series_id'=>$series->id,
                        ]);
                    }
                }
            }
        }
        if ($request->descriptions){
            foreach($request->descriptions as $description){
                SeriesDescription::create([
                    'book_id'=>$book->id,
                    'name'=>$description['name'],
                    'text'=>$description['text'],
                ]);
            }
        }
        return new SingleSeriesResource($series);
    }

    /**
     * Display the specified resource.
     */
    public function show(Series $series)
    {
        return new SingleSeriesResource($series);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSeriesRequest $request, Series $series)
    {
        $this->middleware(['admin']);
        $series->update($request->validated());

        if($request->cover != null){
            File::delete($series->cover);

            $image = time() . '-' . $series->name . '.' . $request->file('cover')->extension();
            $request->cover->move(public_path('series/cover_images'),$image);
            $series->update([
                'cover'=>'series/cover_images/'.$image,
            ]);
        }

        return new SingleSeriesResource($series);
    }


    public function updateImage(Request $request,Series $series){
        $request->validate(['cover'=>'file|image']);
        if (File::exists($series->cover)){
            File::delete(public_path($series->cover));
        }
        $image = time() . '-' . $series->name . '.' . $request->file('cover')->extension();
        $request->cover->move(public_path('series/cover_images'),$image);
        $series->update([
            'cover'=>'series/cover_images/'.$image,
        ]);
        return new SingleSeriesResource($series);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Series $series)
    {
        $this->middleware(['admin']);

        $imagePath = $series->cover;
        Storage::delete($imagePath);

        $series->delete();
        return response()->noContent();
    }
}
