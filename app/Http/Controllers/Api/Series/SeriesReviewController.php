<?php

namespace App\Http\Controllers\Api\Series;

use App\Http\Controllers\Controller;
use App\Http\Resources\Series\SeriesReviewResource;
use App\Models\Series\Series;
use App\Models\Series\SeriesReview;
use Illuminate\Http\Request;

class SeriesReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Series $series)
    {
        return SeriesReviewResource::collection($series->reviews);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Series $series)
    {
        $request->validate([
            'text'=>['required','string'],
            'is_burn'=>['required','boolean']
        ]);

        $seriesReview = SeriesReview::create([
            'series_id'=>$series->id,
            'user_id'=>$request->user()->id,
            'text'=>$request->text,
            'is_burn'=>$request->is_burn,
        ]);
        return new SeriesReviewResource($seriesReview);
    }

    /**
     * Display the specified resource.
     */
    public function show(Series $series,string $review)
    {
        $seriesReview = $series->reviews()->find($review);
        return new SeriesReviewResource($seriesReview);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Series $series,string $review)
    {
        if ($request->text != null){
            $series->reviews()->where('id',$review)
            ->update([
                'text'=>$request->text,
            ]);
        }
        if ($request->is_burn != null){
            $series->reviews()->find($review)
            ->update([
                'text'=>$request->text,
            ]);
        }
        $seriesReview = $series->reviews()->find($review);
        return new SeriesReviewResource($seriesReview);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Series $series, string $review)
    {
        if ($series->reviews()->where('id',$review)->delete()){
            return response()->noContent();
        }
        return response()->json([
            'message'=>'not able to do that'
        ],404);
    }
}
