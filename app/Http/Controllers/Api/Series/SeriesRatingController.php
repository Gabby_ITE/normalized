<?php

namespace App\Http\Controllers\Api\Series;

use App\Http\Controllers\Controller;
use App\Models\Series\Series;
use App\Models\Series\SeriesRating;
use Illuminate\Http\Request;

class SeriesRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Series $series)
    {
        return response()->json([
            'data'=>[$series->ratings()->get()],
        ]);
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Series $series)
    {   $ratings = $series->ratings()->select('rating','user_id')
                        ->where('user_id',$request->user()->id)->get();
        if ($ratings->isNotEmpty()){
            return response()->json([
                'message'=>'is exists',
                'data'=>$ratings,
            ]);
        }
        $request->validate([
            'rating'=>['required','integer','min:1','max:5']
        ]);
        SeriesRating::create([
            'series_id'=>$series->id,
            'user_id'=>$request->user()->id,
            'rating'=>$request->rating
        ]);
        $seriesRating = $series->ratings()->where('series_id',$series->id)
            ->where('user_id',$request->user()->id)->get();
        return response()->json([
            'data'=>$seriesRating
        ]);
    }

    public function update(Request $request, Series $series)
    {
        $request->validate([
            'rating'=>['required','integer','min:1','max:5']
        ]);
        $series->ratings()->where('user_id',$request->user()->id)->update([
            'rating'=>$request->rating
        ]);
        $seriesRating = $series->ratings()->where('user_id',$request->user()->id)
                            ->get();
        return response()->json([
            'data'=>$seriesRating
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Series $series)
    {
        if ($series->ratings()->where('user_id',auth()->user()->id)->delete()){
            return response()->noContent();
        }
        return response()->json(['message'=>'not able to do that'],403);
    }
}
