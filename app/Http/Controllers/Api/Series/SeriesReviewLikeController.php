<?php

namespace App\Http\Controllers\Api\Series;

use App\Http\Controllers\Controller;
use App\Http\Resources\Series\SeriesReviewResource;
use App\Models\Series\Series;
use App\Models\Series\SeriesReviewLike;
use Illuminate\Http\Request;

class SeriesReviewLikeController extends Controller
{
    public function index(Series $series,string $review)
    {
        $seriesReview = $series->reviews()->find($review);
        return response()->json(['data'=>$seriesReview->likes()->count()]);

    }
    public function addLike(Series $series,string $review)
    {
        $seriesReview = $series->reviews()->find($review);
        if ($seriesReview->likes()->where('user_id',auth()->user()->id)->exists()){
            return response()->json([
                'message'=>'you can\'t do that'
            ],403);
        }
        SeriesReviewLike::create([
            'series_review_id'=>$seriesReview->id,
            'user_id'=>auth()->user()->id,
        ]);
        return new SeriesReviewResource($seriesReview);
    }

    public function deleteLike(Series $series,string $review)
    {
        $seriesReview = $series->reviews()->find($review);
        if ($seriesReview->likes()->where('user_id',auth()->user()->id)->exists()){
            $seriesReview->likes()
                    ->where('user_id',auth()->user()->id)->delete();
            return response()->noContent();
        }
        return response()->json([
            'message'=>'you can\'t do that'
        ],403);
    }
}
