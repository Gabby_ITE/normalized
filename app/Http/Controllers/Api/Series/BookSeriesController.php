<?php

namespace App\Http\Controllers\Api\Series;

use App\Http\Resources\Book\ManyBookResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Book\Book;
use App\Models\Series\Series;


class BookSeriesController extends Controller
{
    private function validateThisBook(Series $series,Request $request){
        $request->validate([
            'book_id'=>['required','exists:books,id'],
        ]);
        $book = Book::find($request->book_id);
        $bookSeries = $book->series;
        if($bookSeries){
            $otherSeries = $bookSeries->series;
            if ($otherSeries->id == $series->id){
                return 'this' . $book->name . 'is exists in ' . $series->name;
            }else{
                return 'this' . $book->name . 'is exists in ' . $otherSeries->name;
            }
        }
    }

    public function books(Series $series){
        return ManyBookResource::collection(
            $series->books()->with('book')->get()->pluck('book')->sortByDesc('release_date')
        );
    }


    public function addBook(Request $request, Series $series){
        $message = $this->validateThisBook($series,$request);
        if($message){
            return response()->json(['message'=>$message],401);
        }

        $bookSeries =  $series->books()->create([
            'book_id'=>$request->book_id,
        ]);
        return response()->json([
            'book'=>new ManyBookResource($bookSeries->book),
        ]);
    }
    public function deleteBook(Series $series,string $book){
        $bookSeries = $series->books()->where('book_id', $book)->firstOrFail();
        $bookSeries->delete();
        return response()->noContent();
    }
}
