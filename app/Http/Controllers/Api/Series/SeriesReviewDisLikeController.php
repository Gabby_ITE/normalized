<?php

namespace App\Http\Controllers\Api\Series;

use App\Http\Controllers\Controller;
use App\Http\Resources\Series\SeriesReviewResource;
use App\Models\Series\Series;
use App\Models\Series\SeriesReviewDisLike;
use Illuminate\Http\Request;

class SeriesReviewDisLikeController extends Controller
{
    public function index(Series $series,string $review)
    {
        $seriesReview = $series->reviews()->find($review);
        return response()->json(['data'=>$seriesReview->dis_likes()->count()]);

    }
    public function addDisLike(Series $series,string $review)
    {
        $seriesReview = $series->reviews()->find($review);
        if ($seriesReview->dis_likes()->where('user_id',auth()->user()->id)->exists()){
            return response()->json([
                'message'=>'you can\'t do that'
            ],403);
        }
        SeriesReviewDisLike::create([
            'series_review_id'=>$seriesReview->id,
            'user_id'=>auth()->user()->id,
        ]);
        return new SeriesReviewResource($seriesReview);
    }

    public function deleteDisLike(Series $series,string $review)
    {
        $seriesReview = $series->reviews()->find($review);
        if ($seriesReview->dis_likes()->where('user_id',auth()->user()->id)->exists()){
            $seriesReview->dis_likes()
                    ->where('user_id',auth()->user()->id)->delete();
            return response()->noContent();
        }
        return response()->json([
            'message'=>'you can\'t do that'
        ],403);
    }
}
