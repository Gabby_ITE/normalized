<?php

namespace App\Http\Controllers\Api\Series;

use App\Http\Controllers\Controller;
use App\Http\Resources\Series\SingleSeriesDiscussionResource;
use App\Models\Series\Series;
use App\Models\Series\SeriesDiscussion;
use App\Models\Series\SeriesDiscussionReply;
use Illuminate\Http\Request;

class SeriesDiscussionReplyController extends Controller
{
    public function index(Series $series, string $discussion)
    {
        if ($series->discussions()->where('id',$discussion)->exists() == 0){
            return response()->json(['message'=>'not found'],404);
        }
        $discussion = SeriesDiscussion::find($discussion);
        if ($discussion == null){
            return response()->json(['message'=>'not found'],404);
        }
        return new SingleseriesDiscussionResource($discussion);
    }
    public function store(Request $request,Series $series,string $discussion)
    {
        $request->validate([
            'text'=>['required','string']
        ]);
        if ($series->discussions()->where('id',$discussion)->exists() == 0){
            return response()->json(['message'=>'not found'],404);
        }
        $discussion = SeriesDiscussion::find($discussion);
        if ($discussion == null){
            return response()->json(['message'=>'not found'],404);
        }
        SeriesDiscussionReply::create([
            'user_id'=>$request->user()->id,
            'series_discussion_id'=>$discussion->id,
            'text'=>$request->text,
        ]);
        return new SingleSeriesDiscussionResource($discussion);

    }

    public function store_to_reply(Request $request,Series $series,string $discussion,string $reply)
    {
        $request->validate([
            'text'=>['required','string']
        ]);
        if ($series->discussions()->where('id',$discussion)->exists() == 0){
            return response()->json(['message'=>'not found'],404);
        }
        $discussion = SeriesDiscussion::find($discussion);
        if ($discussion == null){
            return response()->json(['message'=>'not found'],404);
        }
        $reply = SeriesDiscussionReply::find($reply);
        if ($reply == null){
            return response()->json(['message'=>'not found'],404);
        }
        SeriesDiscussionReply::create([
            'user_id'=>$request->user()->id,
            'series_discussion_id'=>$discussion->id,
            'series_discussion_reply_id'=>$reply->id,
            'text'=>$request->text,
        ]);
        return new SingleSeriesDiscussionResource($reply);
    }
    public function show(Series $series,string $discussion,string $reply)
    {
        if ($series->discussions()->where('id',$discussion)){
            return response()->json(['message'=>'not found'],404);
        }
        $discussion = SeriesDiscussion::find($discussion);
        if ($discussion != null){
            $reply = SeriesDiscussionReply::find($reply);
            return new SingleSeriesDiscussionResource($reply);
        }
    }

    public function update(Request $request,Series $series,string $discussion,string $reply)
    {
        $request->validate([
            'text'=>['required','string']
        ]);
        if ($series->discussions()->where('id',$discussion)){
            return response()->json(['message'=>'not found'],404);
        }
        $discussion = SeriesDiscussion::find($discussion);
        if ($discussion == null){
            return response()->json(['message'=>'not found'],404);
        }
        $reply = SeriesDiscussionReply::find($reply);
        if ($reply == null){
            return response()->json(['message'=>'not found'],404);
        }
        if ($reply->user_id == $request->user()->id){
            $reply->update([
                'text'=>$request->text
            ]);
            return new SingleSeriesDiscussionResource($reply);
        }else{
            return response()->json(['message'=>'you are not authorized'],403);
        }


    }
    public function destroy(Series $series,string $discussion,string $reply)
    {
        $reply = SeriesDiscussionReply::find($reply);
        if ($reply == null){
            return response()->json(['message'=>'not found'],404);
        }
        if ($reply->user_id == auth()->user()->id){
            $reply->delete();
            return response()->noContent();
        }else{
            return response()->json(['message'=>'you are not authorized'],403);
        }
    }
}
