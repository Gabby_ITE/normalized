<?php

namespace App\Http\Controllers\Api\Series;

use App\Http\Controllers\Controller;
use App\Http\Resources\Series\ManySeriesDiscussionResource;
use App\Http\Resources\Series\SeriesDiscussionResource;
use App\Http\Resources\Series\SingleSeriesDiscussionResource;
use App\Models\Series\Series;
use App\Models\Series\SeriesDiscussion;
use Illuminate\Http\Request;

class SeriesDiscussionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Series $series)
    {
        return ManySeriesDiscussionResource::collection($series->discussions);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Series $series)
    {
        $request->validate([
            'text'=>['required','string'],
        ]);

        $seriesDiscussion = SeriesDiscussion::create([
            'series_id'=>$series->id,
            'user_id'=>$request->user()->id,
            'text'=>$request->text,
        ]);
        return new SingleSeriesDiscussionResource($seriesDiscussion);
    }

    /**
     * Display the specified resource.
     */
    public function show(Series $series,string $discussion)
    {
        $seriesDiscussion = $series->discussions()->find($discussion);
        return new SingleSeriesDiscussionResource($seriesDiscussion);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Series $series,string $discussion)
    {
        if ($request->text != null){
            $series->discussions()->where('id',$discussion)
            ->update([
                'text'=>$request->text,
            ]);
        }
        $seriesDiscussion = $series->discussions()->find($discussion);
        return new SingleSeriesDiscussionResource($seriesDiscussion);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Series $series, string $discussion)
    {
        if ($series->discussions()->where('id',$discussion)->delete()){
            return response()->noContent();
        }
        return response()->json([
            'message'=>'not able to do that'
        ],404);
    }
}
