<?php

namespace App\Http\Controllers\Api\Series;

use App\Http\Controllers\Controller;
use App\Http\Resources\Series\SeriesDescriptionResource;
use App\Models\Series\Series;
use Illuminate\Http\Request;

class SeriesDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Series $series)
    {
        return SeriesDescriptionResource::collection($series->descriptions);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Series $series)
    {
        $request->validate([
            'text'=>['required','string'],
        ]);
        $name = null;
        if ($request->name != null){
            $name = $request->name;
        }
        $description = $series->descriptions()->create([
            'name'=>$name,
            'text'=>$request->text,
        ]);
        return new SeriesDescriptionResource($description);
    }

    /**
     * Display the specified resource.
     */
    public function show(Series $series,string $description)
    {
        if ($series->descriptions()->find($description)){
            $description = $series->descriptions()->find($description);
            return new SeriesDescriptionResource($description);
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,Series $series, string $description)
    {
        if ($series->descriptions()->find($description)){
            if ($request->name != null){
                $series->descriptions()->find($description)->update([
                    'name'=>$request->name,
                ]);
            }
            if ($request->text != null){
                $series->descriptions()->find($description)->update([
                    'text'=>$request->text,
                ]);
            }
            $description = $series->descriptions()->find($description);
            return new SeriesDescriptionResource($description);
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Series $series, string $description)
    {
        if ($series->descriptions()->find($description)){
            $description = $series->descriptions()->find($description);
            $description->delete();
            return response()->noContent();
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }
}
