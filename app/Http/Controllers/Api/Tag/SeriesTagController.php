<?php

namespace App\Http\Controllers\Api\Tag;

use App\Http\Controllers\Controller;
use App\Http\Resources\TagResource;
use App\Models\Series\Series;
use App\Models\Tag\Tag;
use Illuminate\Http\Request;

class SeriesTagController extends Controller
{
    public function index(Series $series){
        return response()->json([
            'data'=>TagResource::collection($series->tags()->get()->pluck('tag'))
        ]);
    }
    public function storeAll(Request $request, Series $series){
        $tags = collect();
        if ($request->tags){
            // return $request->tags;
            foreach($request->tags as $tag){
                // $tagId = (int) $tagId;
                // $tag = Tag::all()->where('id',$tagId);
                // if ($tag == null || $tag == 0){
                //     continue;
                // }
                if ($series->tags()->where('tag_id',$tag)->get()->isNotEmpty()){
                    continue;
                }

                $tag = $series->tags()->create([
                    'tag_id'=>$tag
                ]);

                $tags->add(new TagResource($tag->tag));
            }
        }
        return TagResource::collection($tags);
    }
    public function store(Request $request,Series $series){
        $request->validate([
            'tag_id'=>['required','exists:tags,id']
        ]);
        if ($series->tags()->where('tag_id',$request->tag_id)->get()->isNotEmpty()){
            return response()->json([
                'message'=>'tag is already added'
            ],403);
        }
        $series->tags()->create([
            'tag_id'=>$request->tag_id
        ]);
        $tag = $series->tags()->get()->pluck('tag')->where('id',$request->tag_id);
        return new TagResource($tag->first());
    }
    public function destroy(Series $series,string $tag){
        $tag = Tag::find($tag);
        foreach ($series->tags()->get() as $seriesTag){
            if ($seriesTag->tag_id == $tag->id){
                $seriesTag->delete();
                return response()->noContent();
            }
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }
}
