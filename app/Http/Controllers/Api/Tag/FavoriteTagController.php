<?php

namespace App\Http\Controllers\Api\Tag;

use App\Http\Controllers\Controller;
use App\Http\Resources\TagResource;
use App\Models\Tag\FavoriteTag;
use App\Models\Tag\Tag;
use Illuminate\Http\Request;

class FavoriteTagController extends Controller
{
    public function index(){
        return response()->json([
            'data'=>TagResource::collection(auth()->user()->tags()->get()->pluck('tag'))
        ]);
    }
    public function storeAll(Request $request){
        $tags = collect();
        if ($request->tags){
            // return $request->tags;
            foreach($request->tags as $tag){
                // $tagId = (int) $tagId;
                // $tag = Tag::all()->where('id',$tagId);
                // if ($tag == null || $tag == 0){
                //     continue;
                // }
                if (auth()->user()->tags()->where('tag_id',$tag)->get()->isNotEmpty()){
                    continue;
                }

                $tag = auth()->user()->tags()->create([
                    'tag_id'=>$tag
                ]);

                $tags->add(new TagResource($tag->tag));
            }
        }
        return TagResource::collection($tags);
    }
    public function store(Request $request){
        $request->validate([
            'tag_id'=>['required','exists:tags,id']
        ]);
        if (auth()->user()->tags()->where('tag_id',$request->tag_id)->get()->isNotEmpty()){
            return response()->json([
                'message'=>'tag is already added'
            ],403);
        }
        $tag = auth()->user()->tags()->create([
            'tag_id'=>$request->tag_id
        ]);
        return new TagResource($tag->tag);
    }
    public function destroy(Tag $tag){
        foreach (auth()->user()->tags()->get() as $favoriteTag){
            if ($favoriteTag->tag_id == $tag->id){
                $favoriteTag->delete();
                return response()->noContent();
            }
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }
}
