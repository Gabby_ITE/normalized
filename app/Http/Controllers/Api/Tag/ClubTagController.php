<?php

namespace App\Http\Controllers\Api\Tag;

use App\Http\Controllers\Controller;
use App\Http\Resources\TagResource;
use App\Models\Club\Club;
use App\Models\Tag\Tag;
use Illuminate\Http\Request;

class ClubTagController extends Controller
{
    public function index(Club $club){
        return response()->json([
            'data'=>TagResource::collection($club->tags()->get()->pluck('tag'))
        ]);
    }

    public function storeAll(Request $request, Club $club){
        $tags = collect();
        if (auth()->user()->id != $club->admin_id){
            return response()->json(['message'=>'your are not admin',403]);
        }
        if ($request->tags){
            foreach($request->tags as $tag){
                if ($club->tags()->where('tag_id',$tag)->get()->isNotEmpty()){
                    continue;
                }

                $tag = $club->tags()->create([
                    'tag_id'=>$tag
                ]);

                $tags->add(new TagResource($tag->tag));
            }
        }
        return TagResource::collection($tags);
    }


    public function store(Request $request,Club $club){
        if ($request->user()->id != $club->admin_id){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        $request->validate([
            'tag_id'=>['required','exists:tags,id']
        ]);
        if ($club->tags()->where('tag_id',$request->tag_id)->get()->isNotEmpty()){
            return response()->json([
                'message'=>'tag is already added'
            ],403);
        }
        $club->tags()->create([
            'tag_id'=>$request->tag_id
        ]);
        $tag = $club->tags()->get()->pluck('tag')->where('id',$request->tag_id);
        return new TagResource($tag->first());
    }
    public function destroy(Club $club,string $tag){
        if (auth()->user()->id != $club->admin_id){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if ($club->tags()->where('tag_id',$tag)->exists()==1){
            $club->tags()->where('tag_id',$tag)->get()->first()->delete();
            return response()->noContent();
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }
}
