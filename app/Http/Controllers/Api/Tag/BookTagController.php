<?php

namespace App\Http\Controllers\Api\Tag;

use App\Http\Controllers\Controller;
use App\Http\Resources\TagResource;
use App\Models\Book\Book;
use App\Models\Tag\Tag;
use Illuminate\Http\Request;

class BookTagController extends Controller
{
    public function index(Book $book){
        return response()->json([
            'data'=>TagResource::collection($book->tags()->get()->pluck('tag'))
        ]);
    }

    public function storeAll(Request $request, Book $book){
        $tags = collect();
        if ($request->tags){
            // return $request->tags;
            foreach($request->tags as $tag){
                // $tagId = (int) $tagId;
                // $tag = Tag::all()->where('id',$tagId);
                // if ($tag == null || $tag == 0){
                //     continue;
                // }
                if ($book->tags()->where('tag_id',$tag)->get()->isNotEmpty()){
                    continue;
                }

                $tag = $book->tags()->create([
                    'tag_id'=>$tag
                ]);

                $tags->add(new TagResource($tag->tag));
            }
        }
        return TagResource::collection($tags);
    }

    public function store(Request $request,Book $book){
        $request->validate([
            'tag_id'=>['required','exists:tags,id']
        ]);
        if ($book->tags()->where('tag_id',$request->tag_id)->get()->isNotEmpty()){
            return response()->json([
                'message'=>'tag is already added'
            ],403);
        }
        $tag = $book->tags()->create([
            'tag_id'=>$request->tag_id
        ]);
        return new TagResource($tag->tag);
    }
    public function destroy(Book $book,string $tag){
        $tag = Tag::find($tag);
        foreach ($book->tags()->get() as $bookTag){
            if ($bookTag->tag_id == $tag->id){
                $bookTag->delete();
                return response()->noContent();
            }
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }
}
