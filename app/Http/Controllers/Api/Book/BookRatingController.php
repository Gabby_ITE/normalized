<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Models\Book\Book;
use App\Models\Book\BookRating;
use Illuminate\Http\Request;

class BookRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Book $book)
    {
        return response()->json([
            'data'=>[$book->ratings()->get()],
        ]);
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Book $book)
    {
        $ratings = $book->ratings()->select('rating','user_id')
                        ->where('user_id',$request->user()->id)->get();
        if ($ratings->isNotEmpty()){
            return response()->json([
                'message'=>'is exists',
                'data'=>$ratings,
            ]);
        }
        $request->validate([
            'rating'=>['required','integer','min:1','max:5']
        ]);
        BookRating::create([
            'book_id'=>$book->id,
            'user_id'=>$request->user()->id,
            'rating'=>$request->rating
        ]);
        $bookRating = $book->ratings()->where('book_id',$book->id)
            ->where('user_id',$request->user()->id)->get();
        return response()->json([
            'data'=>$bookRating
        ]);
    }

    public function update(Request $request, Book $book)
    {
        $request->validate([
            'rating'=>['required','integer','min:1','max:5']
        ]);
        $book->ratings()->where('user_id',$request->user()->id)->update([
            'rating'=>$request->rating
        ]);
        $bookRating = $book->ratings()->where('user_id',$request->user()->id)
                            ->get();
        return response()->json([
            'data'=>$bookRating
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book)
    {
        if ($book->ratings()->where('user_id',auth()->user()->id)->delete()){
            return response()->noContent();
        }
        return response()->json(['message'=>'not able to do that'],403);
    }
}
