<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\BookReviewResource;
use App\Models\Book\Book;
use App\Models\Book\BookReviewDisLike;
use Illuminate\Http\Request;

class BookReviewDisLikeController extends Controller
{

    public function index(Book $book,string $review)
    {
        $bookReview = $book->reviews()->find($review);
        return response()->json(['data'=>$bookReview->dis_likes()->count()]);

    }
    public function addDisLike(Book $book,string $review)
    {
        $bookReview = $book->reviews()->find($review);
        if ($bookReview->dis_likes()->where('user_id',auth()->user()->id)->exists()){
            return response()->json([
                'message'=>'you can\'t do that'
            ],403);
        }
        BookReviewDisLike::create([
            'book_review_id'=>$bookReview->id,
            'user_id'=>auth()->user()->id,
        ]);
        return new BookReviewResource($bookReview);
    }

    public function deleteDisLike(Book $book,string $review)
    {
        $bookReview = $book->reviews()->find($review);
        if ($bookReview->dis_likes()->where('user_id',auth()->user()->id)->exists()){
            $bookReview->dis_likes()
                    ->where('user_id',auth()->user()->id)->delete();
            return response()->noContent();
        }
        return response()->json([
            'message'=>'you can\'t do that'
        ],403);
    }
}
