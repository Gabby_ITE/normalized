<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\BookReviewResource;
use App\Models\Book\Book;
use App\Models\Book\BookReviewLike;
use Illuminate\Http\Request;

class BookReviewLikeController extends Controller
{
    public function index(Book $book,string $review)
    {
        $bookReview = $book->reviews()->find($review);
        return response()->json(['data'=>$bookReview->likes()->count()]);

    }
    public function addLike(Book $book,string $review)
    {
        $bookReview = $book->reviews()->find($review);
        if ($bookReview->likes()->where('user_id',auth()->user()->id)->exists()){
            return response()->json([
                'message'=>'you can\'t do that'
            ],403);
        }
        BookReviewLike::create([
            'book_review_id'=>$bookReview->id,
            'user_id'=>auth()->user()->id,
        ]);
        return new BookReviewResource($bookReview);
    }

    public function deleteLike(Book $book,string $review)
    {
        $bookReview = $book->reviews()->find($review);
        if ($bookReview->likes()->where('user_id',auth()->user()->id)->exists()){
            $bookReview->likes()
                    ->where('user_id',auth()->user()->id)->delete();
            return response()->noContent();
        }
        return response()->json([
            'message'=>'you can\'t do that'
        ],403);
    }
}
