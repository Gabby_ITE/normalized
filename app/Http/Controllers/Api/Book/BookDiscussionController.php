<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\ManyBookDiscussionResource;
use App\Http\Resources\Book\SingleBookDiscussionResource;
use App\Models\Book\Book;
use App\Models\Book\BookDiscussion;
use Illuminate\Http\Request;

class BookDiscussionController extends Controller
{
   /**
     * Display a listing of the resource.
     */
    public function index(Book $book)
    {
        return ManyBookDiscussionResource::collection($book->discussions()->get()->sortByDesc('created_at'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Book $book)
    {
        $request->validate([
            'text'=>['required','string'],
            'book_discussion_id'=>['exists:book_discussions,id']
        ]);

        $bookDiscussion = BookDiscussion::create([
            'book_id'=>$book->id,
            'user_id'=>$request->user()->id,
            'text'=>$request->text,
        ]);
        return new SingleBookDiscussionResource($bookDiscussion);
    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book,string $discussion)
    {
        $bookDiscussion = $book->discussions()->find($discussion);
        return new SingleBookDiscussionResource($bookDiscussion);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Book $book,string $discussion)
    {
        if ($request->text != null){
            $book->discussions()->where('id',$discussion)
            ->update([
                'text'=>$request->text,
            ]);
        }
        $bookDiscussion = $book->discussions()->find($discussion);
        return new SingleBookDiscussionResource($bookDiscussion);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book, string $discussion)
    {
        if ($book->discussions()->where('id',$discussion)->delete()){
            return response()->noContent();
        }
        return response()->json([
            'message'=>'not able to do that'
        ],404);
    }
}
