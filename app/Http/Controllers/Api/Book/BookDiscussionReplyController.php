<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\SingleBookDiscussionResource;
use App\Models\Book\Book;
use App\Models\Book\BookDiscussion;
use App\Models\Book\BookDiscussionReply;
use Illuminate\Http\Request;

class BookDiscussionReplyController extends Controller
{

    public function store(Request $request,Book $book,string $discussion)
    {
        $request->validate([
            'text'=>'required'
        ]);
        $discussion = BookDiscussion::find($discussion);
        if ($discussion == null){
            return response()->json(['message'=>'not found'],404);
        }
        BookDiscussionReply::create([
            'user_id'=>$request->user()->id,
            'book_discussion_id'=>$discussion->id,
            'text'=>$request->text,
        ]);
        return new SingleBookDiscussionResource($discussion);

    }

    public function store_to_reply(Request $request,Book $book,string $discussion,string $reply)
    {
        $request->validate([
            'text'=>['required','string']
        ]);
        if ($book->discussions()->where('id',$discussion)->exists() == 0){
            return response()->json(['message'=>'discussion not found'],404);
        }
        $discussion = BookDiscussion::find($discussion);
        if ($discussion == null){
            return response()->json(['message'=>'discussion not found'],404);
        }
        $reply = BookDiscussionReply::find($reply);
        if ($reply == null){
            return response()->json(['message'=>'reply not found'],404);
        }
        BookDiscussionReply::create([
            'user_id'=>$request->user()->id,
            'book_discussion_id'=>$discussion->id,
            'book_discussion_reply_id'=>$reply->id,
            'text'=>$request->text,
        ]);
        return new SingleBookDiscussionResource($reply);
    }

    public function show(Book $book,string $discussion,string $reply)
    {
        if ($book->discussions()->where('id',$discussion)->exists() == 0){
            return response()->json(['message'=>'not found'],404);
        }
        $discussion = BookDiscussion::find($discussion);
        if ($discussion != null){
            $reply = BookDiscussionReply::find($reply);
            return new SingleBookDiscussionResource($reply);
        }
    }

    public function update(Request $request,Book $book,string $discussion,string $reply)
    {
        $request->validate([
            'text'=>['required','string']
        ]);
        if ($book->discussions()->where('id',$discussion)->exists() == 0){
            return response()->json(['message'=>'not found'],404);
        }
        $discussion = BookDiscussion::find($discussion);
        if ($discussion == null){
            return response()->json(['message'=>'not found'],404);
        }
        $reply = BookDiscussionReply::find($reply);
        if ($reply == null){
            return response()->json(['message'=>'not found'],404);
        }
        if ($reply->user_id == $request->user()->id){
            $reply->update([
                'text'=>$request->text
            ]);
            return new SingleBookDiscussionResource($reply);
        }else{
            return response()->json(['message'=>'you are not authorized'],403);
        }


    }
    public function destroy(Book $book,string $discussion,string $reply)
    {
        $reply = BookDiscussionReply::find($reply);
        if ($reply == null){
            return response()->json(['message'=>'not found'],404);
        }
        if ($reply->user_id == auth()->user()->id){
            $reply->delete();
            return response()->noContent();
        }else{
            return response()->json(['message'=>'you are not authorized'],403);
        }
    }
}
