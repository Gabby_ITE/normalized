<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\BookReviewResource;
use App\Models\Book\Book;
use App\Models\Book\BookReview;
use Illuminate\Http\Request;

class BookReviewContorller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Book $book)
    {
        return BookReviewResource::collection($book->reviews);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Book $book)
    {
        $request->validate([
            'text'=>['required','string'],
            'is_burn'=>['required','boolean']
        ]);

        $bookReview = BookReview::create([
            'book_id'=>$book->id,
            'user_id'=>$request->user()->id,
            'text'=>$request->text,
            'is_burn'=>$request->is_burn,
        ]);
        return new BookReviewResource($bookReview);
    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book,string $review)
    {
        $bookReview = $book->reviews()->find($review);
        return new BookReviewResource($bookReview);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Book $book,string $review)
    {
        if ($request->text != null){
            $book->reviews()->where('id',$review)
            ->update([
                'text'=>$request->text,
            ]);
        }
        if ($request->is_burn != null){
            $book->reviews()->find($review)
            ->update([
                'is_burn'=>$request->is_burn,
            ]);
        }
        $bookReview = $book->reviews()->find($review);
        return new BookReviewResource($bookReview);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book, string $review)
    {
        if ($book->reviews()->where('id',$review)->delete()){
            return response()->noContent();
        }
        return response()->json([
            'message'=>'not able to do that'
        ],404);
    }
}
