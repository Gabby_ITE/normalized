<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\BookDescriptionResource;
use App\Models\Book\Book;
use Illuminate\Http\Request;

class BookDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Book $book)
    {
        return BookDescriptionResource::collection($book->descriptions);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Book $book)
    {
        $request->validate([
            'text'=>['required','string'],
        ]);
        $name = null;
        if ($request->name != null){
            $name = $request->name;
        }
        $description = $book->descriptions()->create([
            'name'=>$name,
            'text'=>$request->text,
        ]);
        return new BookDescriptionResource($description);
    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book,string $description)
    {
        if ($book->descriptions()->find($description)){
            $description = $book->descriptions()->find($description);
            return new BookDescriptionResource($description);
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,Book $book, string $description)
    {
        if ($book->descriptions()->find($description)){
            if ($request->name != null){
                $book->descriptions()->find($description)->update([
                    'name'=>$request->name,
                ]);
            }
            if ($request->text != null){
                $book->descriptions()->find($description)->update([
                    'text'=>$request->text,
                ]);
            }
            $description = $book->descriptions()->find($description);
            return new BookDescriptionResource($description);
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book, string $description)
    {
        if ($book->descriptions()->find($description)){
            $description = $book->descriptions()->find($description);
            $description->delete();
            return response()->noContent();
        }
        return response()->json([
            'message'=>'not found'
        ],404);
    }
}
