<?php

namespace App\Http\Controllers\Api\Book;

use App\Http\Controllers\Controller;
use App\Http\Requests\Book\StoreBookRequest;
use App\Http\Requests\Book\UpdateBookRequest;
use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\Book\SingleBookResource;
use App\Models\Author\Author;
use App\Models\Author\AuthorBook;
use App\Models\Book\Book;
use App\Models\Book\BookDescription;
use App\Models\Tag\BookTag;
use App\Models\Tag\FavoriteTag;
use App\Models\Tag\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Ramsey\Collection\Collection;
use Smalot\PdfParser\Parser;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (auth()->user()->role == 'admin'){
            return ManyBookResource::collection(Book::all()->sortByDesc('release_date'));
        }
        $books = [];
        $counter = 0;
        foreach(auth()->user()->tags()->get() as $tag){
            $books[$counter]  = ManyBookResource::collection(
                BookTag::where('tag_id',$tag->tag_id)->get()
                        ->pluck('book')->where('is_private',false)->sortByDesc('release_date')
            );
            $counter++;
        }

        return collect($books);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBookRequest $request)
    {
        $this->middleware(['admin']);
        $request->validated();
        $is_private = 0;
        if ($request->user()->role != 'admin'){
            $is_private = 1;
        }

        $image = time() . '-' . $request->title . '.' . $request->file('cover')->extension();
        $request->cover->move(public_path('books/cover_images'),$image);
        $image='books/cover_images/'.$image;

        $file = time() . '-' . $request->title . '.' . $request->file('file')->extension();
        $request->file->move(public_path('books/files'),$file);
        $file='books/files/'.$file;

        $parser = new Parser();
        $pdf = $parser->parseFile($file);

        $book = Book::create([
            'user_id'=>$request->user()->id,
            'title'=>$request->title,
            'cover'=>$image,
            'file'=>$file,
            'number_of_pages'=>count($pdf->getPages()),
            'release_date'=>$request->release_date,
            'is_private'=>$is_private,
        ]);

        if ($request->authors){
            foreach($request->authors as $authorId){
                $author = Author::all()->find($authorId);
                $exits = false;
                if ($author){
                    foreach ($book->authors()->get() as $authorBook){
                        if ($authorBook->author->id == $author->id){
                            $exits = true;
                            break;
                        }
                    }
                    if(!$exits){
                        AuthorBook::create([
                            'author_id'=> $author->id,
                            'book_id'=>$book->id,
                        ]);
                    }
                }
            }
        }

        if ($request->tags){
            foreach($request->tags as $tagId){
                $tag = Tag::all()->find($tagId);
                $exits = false;
                if ($tag){
                    foreach ($book->tags()->get() as $bookTag){
                        if ($bookTag->tag->id == $tag->id){
                            $exits = true;
                            break;
                        }
                    }
                    if(!$exits){
                        BookTag::create([
                            'tag_id'=> $tag->id,
                            'book_id'=>$book->id,
                        ]);
                    }
                }
            }
        }

        if ($request->descriptions){
            foreach($request->descriptions as $description){
                BookDescription::create([
                    'book_id'=>$book->id,
                    'name'=>$description['name'],
                    'text'=>$description['text'],
                ]);
            }
        }

        return new SingleBookResource($book);
    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book)
    {
        return new SingleBookResource($book);
    }

    public function getFile(Book $book){
        if ($book->exists())
            return response()->json(['data'=>$book->file]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBookRequest $request, Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if (auth()->user()->role == 'admin'){
            if ($request->is_private != null){
                $book->update(['is_private'=>$request->is_private]);
            }
        }
        if($request->image != null){
            if (File::exists($book->cover)){
                File::delete(public_path($book->cover));
            }
            $image = time() . '-' . $book->title . '.' . $request->file('cover')->extension();
            $request->cover->move(public_path('books/cover_images'),$image);
            $book->update([
                'cover'=>'books/cover_images/'.$image,
            ]);
        }
        if($request->file != null){

            if (File::exists($book->file)){
                File::delete(public_path($book->file));
            }
            $file = time() . '-' . $book->title . '.' . $request->file('file')->extension();
            $request->file->move(public_path('books/files'),$file);
            $book->update([
                'file'=>'books/files/'.$file,
            ]);
            $parser = new Parser();
            $pdf = $parser->parseFile($book->file);

            $book->update(['number_of_pages'=>count($pdf->getPages())]);
        }

        return new SingleBookResource($book);
    }

    public function updateImage(Request $request,Book $book){
        $request->validate(['cover'=>'file|image','file'=>'file']);
        if (File::exists($book->cover)){
            File::delete(public_path($book->cover));
        }
        $image = time() . '-' . $book->title . '.' . $request->file('cover')->extension();
        $request->cover->move(public_path('books/cover_images'),$image);
        $book->update([
            'cover'=>'books/cover_images/'.$image,
        ]);
        if (File::exists($book->file)){
            File::delete(public_path($book->file));
        }
        $file = time() . '-' . $book->title . '.' . $request->file('file')->extension();
        $request->file->move(public_path('books/files'),$file);
        $book->update([
            'file'=>'books/files/'.$file,
        ]);
        return new SingleBookResource($book);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book)
    {
        if ($book->user_id != auth()->user()->id || auth()->user()->role != 'admin'){
            return response()->json(['message'=>'you can\'t do that'],403);
        }

        Storage::delete($book->cover);

        Storage::delete($book->file);

        $book->delete();
        return response()->noContent();
    }

    public function suggests(Book $book){
        $authors = $book->authors->pluck('author');
        $authors_books = collect();
        foreach($authors as $author){
            if ($authors_books->isEmpty()){
                $authors_books = collect($author->books->pluck('book'));
                continue;
            }
            $authors_books->merge(collect($author->books->pluck('book')));
        }
        $authors_books = $authors_books->countBy('id')->sortDesc()->keys();
        $tags = $book->tags->pluck('tag')->unique('id');
        $tags_books = collect();
        foreach($tags as $tag){
            if ($tags_books->isEmpty()){
                $tags_books = collect($tag->books->pluck('book'));
                continue;
            }
            $tags_books->merge(collect($tag->books->pluck('book')));
        }
        $tags_books = $tags_books->countBy('id')->sortDesc()->keys();;
        $books = collect();
        foreach($authors_books->merge($tags_books) as $id){
            $books->add(Book::find($id));
        }
        return ManyBookResource::collection($books);
    }
}
