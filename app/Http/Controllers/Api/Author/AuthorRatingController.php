<?php

namespace App\Http\Controllers\Api\Author;

use App\Http\Controllers\Controller;
use App\Models\Author\Author;
use App\Models\Author\AuthorRating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthorRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Author $author)
    {
        return response()->json([
            'data'=>[$author->ratings()->get()],
        ]);
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request,Author $author)
    {   $ratings = $author->ratings()->select('rating','user_id')
                        ->where('user_id',$request->user()->id)->get();
        if ($ratings->isNotEmpty()){
            return response()->json([
                'message'=>'is exists',
                'data'=>$ratings,
            ]);
        }
        $request->validate([
            'rating'=>['required','integer','min:1','max:5']
        ]);
        AuthorRating::create([
            'author_id'=>$author->id,
            'user_id'=>$request->user()->id,
            'rating'=>$request->rating
        ]);
        $authorRating = $author->ratings()->where('author_id',$author->id)
            ->where('user_id',$request->user()->id)->get();
        return response()->json([
            'data'=>$authorRating
        ]);
    }

    public function update(Request $request, Author $author)
    {
        $request->validate([
            'rating'=>['required','integer','min:1','max:5']
        ]);
        $author->ratings()->where('user_id',$request->user()->id)->update([
            'rating'=>$request->rating
        ]);
        $authorRating = $author->ratings->where('user_id',$request->user()->id);
        return response()->json([
            'data'=>$authorRating
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Author $author)
    {
        if ($author->ratings()->where('user_id',auth()->user()->id)->delete()){
            return response()->noContent();
        }
        return response()->json(['message'=>'not able to do that'],403);
    }
}
