<?php

namespace App\Http\Controllers\Api\Author;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\Book\SingleBookResource;
use App\Models\Author\Author;
use App\Models\Author\AuthorBook;
use App\Models\Book\Book;
use Illuminate\Http\Request;


class AuthorBookController extends Controller
{
    public function books(Author $author)
    {
        $authorBooks =  $author->books->pluck('book')->sortByDesc('release_date');
        return ManyBookResource::collection($authorBooks);
    }

    public function addBook(Author $author,Request $request)
    {
        $request->validate([
            'book_id'=>['required','exists:books,id']
        ]);
        $book = Book::all()->find($request->book_id);
        foreach ($author->books()->get() as $authorBook){
            if ($authorBook->book->id == $book->id){
                return response()->json(['message'=>'book is already there']);
            }
        }
        $authorBook = AuthorBook::create([
            'author_id'=> $author->id,
            'book_id'=>$request->book_id,
        ]);
        return response()->json([
            'data'=>new SingleBookResource($authorBook->book),
        ]);
    }
    public function deleteBook(Author $author,string $book)
    {
        $book = Book::find($book);
        foreach($author->books()->get() as $authorBook){
            if($authorBook->book->id == $book->id){
                $authorBook->delete();
                return response()->noContent();
            }
        }
        return response()->json([
            'message'=>'book not found'
        ],404);
    }
}
