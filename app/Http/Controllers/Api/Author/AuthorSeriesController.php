<?php

namespace App\Http\Controllers\Api\Author;

use App\Http\Controllers\Controller;
use App\Http\Resources\Series\ManySeriesResource;
use App\Http\Resources\Series\SingleSeriesResource;
use App\Models\Author\Author;
use App\Models\Author\AuthorSeries;
use App\Models\Series\Series;
use Illuminate\Http\Request;

class AuthorSeriesController extends Controller
{
    public function series(Author $author)
    {
        $series =  $author->series->pluck('series')->sortByDesc('release_date');
        return response()->json([
            'data'=>ManySeriesResource::collection($series),
        ]);
    }

    public function addSeries(Author $author,Request $request)
    {
        $request->validate([
            'series_id'=>['required','exists:series,id']
        ]);
        $Series = Series::all()->find($request->series_id);
        foreach ($author->series()->get() as $authorSeries){
            if ($authorSeries->series->id == $Series->id){
                return response()->json(['message'=>'Series is already there']);
            }
        }
        $authorSeries = AuthorSeries::create([
            'author_id'=> $author->id,
            'series_id'=>$request->series_id,
        ]);
        return response()->json([
            'data'=>new SingleSeriesResource($authorSeries->series),
        ]);
    }

    public function deleteSeries(Author $author,string $series)
    {
        $series = Series::all()->find($series);;
        foreach($author->series()->get() as $authorSeries){
            if($authorSeries->series->id == $series->id){
                $authorSeries->delete();
                return response()->noContent();
            }
        }
        return response()->json([
            'message'=>'series not found'
        ],404);

    }
}
