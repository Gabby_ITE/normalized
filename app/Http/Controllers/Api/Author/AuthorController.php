<?php

namespace App\Http\Controllers\Api\Author;

use App\Http\Controllers\Controller;
use App\Http\Requests\Author\StoreAuthorRequest;
use App\Http\Requests\Author\UpdateAuthorRequest;
use App\Http\Resources\Author\ManyAuthorResource;
use App\Http\Resources\Author\SingleAuthorResource;
use App\Http\Resources\AuthorResource;
use App\Models\Author\Author;
use Illuminate\Support\Facades\Storage;

class AuthorController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ManyAuthorResource::collection(Author::select('id', 'first_name','last_name','image')->get());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAuthorRequest $request)
    {
        $this->middleware(['admin']);
        $author = Author::create([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name
        ]);
        if ($request->image){
            $author->image = $request->file('image')->store('authors_images');
        }
        return new SingleAuthorResource($author);
    }

    /**
     * Display the specified resource.
     */
    public function show(Author $author)
    {
        return new SingleAuthorResource($author);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAuthorRequest $request, Author $author)
    {
        $this->middleware(['admin']);
        $author->update($request->validated());
        if ($request->image){
            Storage::delete($author->image);
            $author->image = $request->file('image')->store('authors_images');
        }
        return new SingleAuthorResource($author);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Author $author)
    {
        $this->middleware(['admin']);
        Storage::delete($author->image);
        $author->delete();
        return response()->noContent();
    }
}
