<?php

namespace App\Http\Controllers\Api\Author;

use App\Http\Controllers\Controller;
use App\Http\Resources\Author\ManyAuthorResource;
use App\Http\Resources\User\ManyUserResource;
use App\Models\Author\Author;
use App\Models\Author\FavoriteAuthor;
use Illuminate\Http\Request;

class FavoriteAuthorController extends Controller
{
    public function index()
    {
        return ManyUserResource::collection(auth()->user()->authors->pluck('author'));
    }

    public function storeAll(Request $request){
        $authors = collect();
        if ($request->authors){
            // return $request->authors;
            foreach($request->authors as $author){
                if (auth()->user()->authors()->where('author_id',$author)->get()->isNotEmpty()){
                    continue;
                }
                if (!Author::where('id',$author)->exists()){
                    continue;
                }
                // return $author;
                $author = FavoriteAuthor::create([
                    'user_id'=>auth()->user()->id,
                    'author_id'=>$author
                ]);

                $authors->add(new ManyAuthorResource($author->author));
            }
        }
        return ManyAuthorResource::collection($authors);
    }
    public function store(Request $request)
    {
        $request->validate([
            'author_id'=>['required','exists:authors,id']
        ]);
        if ($request->user()->authors()->where('author_id',$request->author_id)->exists()==1){
            return response()->json(['message'=>'you already added'],403);
        }
        $favoriteAuthor =  FavoriteAuthor::create([
            'user_id'=>$request->user()->id,
            'author_id'=>$request->author_id,
        ]);
        return response()->json(['data'=>$favoriteAuthor]);
    }
    public function destroy(Author $author)
    {
        if (auth()->user()->authors()->where('author_id',$author->id)->exists() == 1){
            auth()->user()->authors()->where('author_id',$author->id)->first()->delete();
            return response()->noContent();
        }
        return response()->json(['message'=>'not found'],404);
    }
}
