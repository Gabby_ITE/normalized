<?php

namespace App\Http\Controllers\Api\Request;

use App\Http\Controllers\Controller;
use App\Http\Resources\Request\ManyRequestResource;
use App\Http\Resources\Request\SingleRequestResource;
use App\Models\Request\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        if (auth()->user()->role == 'admin'){
            return ManyRequestResource::collection(
                auth()->user()->received_reports->sortBy('is_read')
                ->sortBy('is_done')->sortByDesc('created_at')
            );
        }
        return ManyRequestResource::collection(
            auth()->user()->reports->sortBy('is_read')
            ->sortBy('is_done')->sortByDesc('created_at')
        );
    }
    public function store(Request $request)
    {
        $request->validate([
            'message'=>['required','string'],
            'reported_type'=>[
                'required',
                'in:series_review,book_review,book_discussion,book_discussion_reply,
                series_discussion,series_discussion_reply,club_post,club_post_reply'
            ]
        ]);
        if ($request->reported_type == 'series_review'){
            $request->validate(['reported_id'=>['required','exists:series_reviews,id']]);
        }
        if ($request->reported_type == 'book_review'){
            $request->validate(['reported_id'=>['required','exists:book_reviews,id']]);
        }
        if ($request->reported_type == 'book_discussion'){
            $request->validate(['reported_id'=>['required','exists:book_discussions,id']]);
        }
        if ($request->reported_type == 'book_discussion_reply'){
            $request->validate(['reported_id'=>['required','exists:book_discussion_replies,id']]);
        }

        if ($request->reported_type == 'series_discussion'){
            $request->validate(['reported_id'=>['required','exists:series_discussions,id']]);
        }
        if ($request->reported_type == 'series_discussion_reply'){
            $request->validate(['reported_id'=>['required','exists:series_discussion_replies,id']]);
        }
        if ($request->reported_type == 'club_post'){
            $request->validate(['reported_id'=>['required','exists:club_posts,id']]);
        }
        if ($request->reported_type == 'club_post_reply'){
            $request->validate(['reported_id'=>['required','exists:club_post_replies,id']]);
        }
        if ($this->checkReportExists($request)){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        $report = Report::create([
            'sender_id'=>$request->user()->id,
            'receiver_id'=>1,
            'message'=>$request->message,
            'reported_type'=>$request->reported_type,
            'reported_id'=>$request->reported_id,
        ]);

        return new SingleRequestResource($report);
    }
    private function checkReportExists(Request $request)
    {
        return $request->user()->reports()->where('receiver_id',1)
                ->where('reported_type',$request->reported_type)
                ->where('reported_id',$request->reported_id)->exists() == 1;
    }
    public function show(Report $report)
    {
        if (auth()->user()->id == $report->sender_id || auth()->user()->role == 'admin'){
            if (auth()->user()->role == 'admin'){
                $report->update(['is_read'=>true]);
            }
            return new SingleRequestResource($report);
        }
        return response()->json(['message'=>'you can\'t do that'],403);
    }

    public function destroy(Report $report)
    {
        if ((auth()->user()->id == $report->sender_id
            || auth()->user()->role == 'admin')
            && $report->is_done == false){
            $report->delete();
            return response()->noContent();
        }
        return response()->json(['message'=>'you can\'t do that'],403);
    }
}
