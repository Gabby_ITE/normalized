<?php

namespace App\Http\Controllers\Api\Request;

use App\Http\Controllers\Controller;
use App\Http\Resources\Book\ManyBookResource;
use App\Http\Resources\Request\ManyRequestResource;
use App\Http\Resources\Request\SingleRequestResource;
use App\Models\Request\BookRequest;
use App\Models\Request\FriendRequest;
use Illuminate\Http\Request;

class BookRequestController extends Controller
{
    public function index()
    {
        if (auth()->user()->role == 'admin'){
            return ManyRequestResource::collection(auth()->user()->received_book_requests);

        }
        return ManyRequestResource::collection(
            auth()->user()->book_requests
            ->sortBy('is_read')->sortBy('is_done')->sortByDesc('created_at')
        );
    }
    public function store(Request $request)
    {
        $request->validate([
            'title'=>['required'],
            'author_name'=>['required'],
        ]);

        $bookRequest = BookRequest::create([
            'sender_id'=>$request->user()->id,
            'receiver_id'=>1,
            'title'=>$request->title,
            'author_name'=>$request->author_name,
        ]);
        return new SingleRequestResource($bookRequest);
        // return response()->json(['data'=>$bookRequest]);
    }
    public function show(BookRequest $request)
    {
        if ($request->sender_id == auth()->user()->id
            || $request->receiver_id == auth()->user()->id
            || auth()->user()->role == 'admin'){
                if ($request->receiver_id == auth()->user()->id){
                    $request->update(['is_read'=>true]);
                }
                return new SingleRequestResource($request);
        }
        return response()->json(['message'=>'your can\'t do that'],403);
    }
    public function update(Request $req,BookRequest $request)
    {
        if ($request->sender_id == $req->user()->id && $request->is_done == 0){
            if ($req->title){
                $request->update(['title'=>$req->title]);
            }
            if ($req->author_name){
                $request->update(['author_name'=>$req->author_name]);
            }
            return new SingleRequestResource($request);
        }

        return response()->json(['message'=>'you can\'t do that'],403);
    }

    public function destroy(BookRequest $request)
    {
        if ($request->sender_id == auth()->user()->id
            || $request->receiver_id == auth()->user()->id
            || auth()->user()->role == 'admin'){
                $request->delete();
                return response()->noContent();
        }
        return response()->json(['message'=>'your can\'t do that'],403);
    }

}
