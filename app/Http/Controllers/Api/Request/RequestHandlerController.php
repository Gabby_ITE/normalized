<?php

namespace App\Http\Controllers\Api\Request;

use App\Http\Controllers\Controller;
use App\Http\Resources\Request\ClubRequestResource;
use App\Http\Resources\Request\FriendRequestResource;
use App\Http\Resources\Request\ResponseResource;
use App\Http\Resources\Request\SingleRequestResource;
use App\Models\Club\ClubMember;
use App\Models\Friend;
use App\Models\Request\BookRequest;
use App\Models\Request\ClubRequest;
use App\Models\Request\FriendRequest;
use App\Models\Request\Report;
use App\Models\Request\Response;
use Illuminate\Http\Request;

use function PHPSTORM_META\type;

class RequestHandlerController extends Controller
{

    public function friend(Request $request,string $friendRequest)
    {
        $friendRequest = FriendRequest::find($friendRequest);
        // $friendRequest->update[['is_read'=>false]];
        if ($friendRequest == null){
            return response()->json(['message'=>'you can\'t do that null'],403);
        }
        if (Response::where('request_type','friend')->where('request_id',$friendRequest->id)->exists()){
            return response()->json(['message'=>'you can\'t do that has a response'],403);
        }
        if ($request->user()->id != $friendRequest->receiver_id){
            return response()->json(['message'=>'you are not the receiver'],403);
        }
        if ($friendRequest->is_done){
            return response()->json(['message'=>'its already done'],403);
        }
        $request->validate(['type'=>['required','in:accept,reject']]);
        $friendRequest->update(['is_done'=>true]);
        if ($request->type == 'accept'){

            if (Friend::where('sender_id',$friendRequest->sender_id)
                    ->where('receiver_id',$friendRequest->receiver_id)->exists()
                || Friend::where('receiver_id',$friendRequest->sender_id)
                    ->where('sender_id',$friendRequest->receiver_id)->exists()){
                    return response()->json(['message'=>' you can not do that'],403);
            }
            Friend::create([
                'sender_id'=>$friendRequest->sender_id,
                'receiver_id'=>$friendRequest->receiver_id,
            ]);
        }

        Response::create([
            'sender_id'=>$request->user()->id,
            'receiver_id'=>$friendRequest->sender_id,
            'type'=>$request->type,
            'request_type'=>'friend',
            'request_id'=>$friendRequest->id,
        ]);
        // $friendRequest->update[['is_read'=>false]];
        return new FriendRequestResource($friendRequest);
    }

    public function club(Request $request, string $clubRequest)
    {
        $clubRequest = ClubRequest::find($clubRequest);
        if ($clubRequest == null ){
            return response()->json(['message'=>'you can\'t do that'],403);
        }

        if ($clubRequest->response()->where('request_type','club')->exists()){
            return response()->json(['message'=>'you can\'t do that has a response'],403);
        }
        if ($request->user()->id != $clubRequest->receiver_id){
            return response()->json(['message'=>'you are not the receiver'],403);
        }
        if ($clubRequest->is_done){
            return response()->json(['message'=>'its already done'],403);
        }
        $request->validate(['type'=>['required','in:accept,reject']]);
        $clubRequest->update(['is_done'=>true]);
        $response = null;
        if ($request->response == 'accept'){
            ClubMember::create([
                'user_id'=>$clubRequest->sender_id,
                'club_id'=>$clubRequest->club_id,
            ]);
            $response = Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$clubRequest->sender_id,
                'type'=>$request->type,
                'request_type'=>'club',
                'request_id'=>$clubRequest->id,
            ]);
        }else{
            $response = Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$clubRequest->sender_id,
                'type'=>$request->type,
                'request_type'=>'club',
                'request_id'=>$clubRequest->id,
            ]);
        }
        $clubRequest->update(['is_read'=>false]);
        return new ClubRequestResource($clubRequest);
    }

    public function book(Request $request, string $bookRequest)
    {
        $bookRequest = BookRequest::find($bookRequest);
        if ($bookRequest == null || Response::where('request_id',$bookRequest->id)->where('request_type','book')->exists()){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        if ($request->user()->id != $bookRequest->receiver_id){
            return response()->json(['message'=>'you are not the receiver'],403);
        }
        $request->validate(['type'=>['required','in:accept,reject']]);
        $bookRequest->update(['is_done'=>true]);
        $response = null;
        if ($request->response == 'accept'){
            $response = Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$bookRequest->sender_id,
                'type'=>$request->type,
                'request_type'=>'book',
                'request_id'=>$bookRequest->id,
            ]);
        }else{
            $response = Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$bookRequest->sender_id,
                'type'=>$request->type,
                'request_type'=>'book',
                'request_id'=>$bookRequest->id,
            ]);
        }
        $bookRequest->update(['is_read'=>false]);
        return new SingleRequestResource($bookRequest);
    }

    public function report(Request $request, string $report)
    {
        $report = Report::find($report);
        if ($report == null || Response::where('request_id',$report->id)->where('request_type','report')->exists()){
            return response()->json(['message'=>'you can\'t do that'],403);
        }
        $request->validate(['type'=>['required','in:accept,reject']]);
        $report->update(['is_done'=>true]);
        if ($request->type == 'accept'){
            $report->reported->delete();
            Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$report->sender_id,
                'type'=>$request->type,
                'request_type'=>'report',
                'request_id'=>$report->id,
            ]);
        }else{
            Response::create([
                'sender_id'=>$request->user()->id,
                'receiver_id'=>$report->sender_id,
                'type'=>$request->type,
                'request_type'=>'report',
                'request_id'=>$report->id,
            ]);
        }
        $report->update(['is_read'=>false]);
        return new SingleRequestResource($report);
    }

}
