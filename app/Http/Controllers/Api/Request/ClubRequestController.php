<?php

namespace App\Http\Controllers\Api\Request;

use App\Http\Controllers\Controller;
use App\Http\Resources\Club\ManyClubResource;
use App\Http\Resources\Request\ClubRequestResource;
use App\Models\Club\Club;
use App\Models\Request\ClubRequest;
use Illuminate\Http\Request;

class ClubRequestController extends Controller
{
    public function index()
    {
        $sends = auth()->user()->send_club_requests;
        $receives = auth()->user()->receive_club_requests;
        $full = $sends->union($receives)->sortBy('is_read')
                ->sortBy('is_done')->sortByDesc('created_at');

        return ClubRequestResource::collection($full);
    }
    public function received_requests()
    {
        $receives = auth()->user()->receive_club_requests->sortBy('is_read')
                    ->sortByDesc('created_at')->sortBy('is_done');
        return ClubRequestResource::collection($receives);
    }
    public function sended_requests()
    {
        $sends = auth()->user()->send_club_requests->sortBy('is_read')
                ->sortByDesc('created_at')->sortBy('is_done');
        return ClubRequestResource::collection($sends);
    }
    public function store(Request $request)
    {
        $request->validate([
            'club_id'=>['required','exists:clubs,id']
        ]);
        $club = Club::find($request->club_id);
        if ($this->checkClubExists($request,$club)){
            return response()->json(['message'=>'you can\'t do that']);
        }
        $clubRequest = ClubRequest::create([
            'sender_id'=>$request->user()->id,
            'receiver_id'=>$club->admin_id,
            'club_id'=>$club->id
        ]);
        return new ClubRequestResource($clubRequest);
    }

    private function checkClubExists(Request $request,Club $club)
    {
        if ($club->members()->where('user_id',$request->user()->id)->exists() == 1){
            return true;
        }
        return $request->user()->send_club_requests()->where('receiver_id',$club->admin_id)
                ->exists() == 1;
    }

    public function show(ClubRequest $request)
    {
        if ($request->sender_id == auth()->user()->id
            || $request->receiver_id == auth()->user()->id
            || auth()->user()->role == 'admin'){
                if ($request->receiver_id == auth()->user()->id){
                    $request->update(['is_read'=>true]);
                }
                return new ClubRequestResource($request);
        }
        return response()->json(['message'=>'your can\'t do that'],403);
    }

    public function destroy(ClubRequest $request)
    {
        if ($request->sender_id == auth()->user()->id
            || $request->receiver_id == auth()->user()->id
            || auth()->user()->role == 'admin'){
                $request->delete();
                return response()->noContent();
        }
        return response()->json(['message'=>'your can\'t do that'],403);
    }
}
