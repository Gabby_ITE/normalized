<?php

namespace App\Http\Controllers\Api\Request;

use App\Http\Controllers\Controller;
use App\Http\Resources\Request\FriendRequestResource;
use App\Http\Resources\User\FriendResource;
use App\Models\Request\FriendRequest;
use Illuminate\Http\Request;

class FriendRequestController extends Controller
{
    public function index()
    {
        $sends = auth()->user()->send_friend_requests;
        $receives = auth()->user()->receive_friend_requests;
        $full = $sends->union($receives)->sortByDesc('created_at')->sortBy('is_done');

        return FriendRequestResource::collection($full);
    }

    public function received_requests()
    {
        $receives = auth()->user()->receive_friend_requests->sortByDesc('created_at')->sortBy('is_done');
        return FriendRequestResource::collection($receives);
    }
    public function sended_requests()
    {
        $sends = auth()->user()->send_friend_requests->sortByDesc('created_at')->sortBy('is_done');
        return FriendRequestResource::collection($sends);
    }
    public function store(Request $request)
    {
        $request->validate([
            'receiver_id'=>['required','exists:users,id']
        ]);

        if ($this->checkFriendExists($request)){
            return response()->json(['message'=>'you can\'t do that ']);
        }

        $friendRequest = FriendRequest::create([
            'sender_id'=>$request->user()->id,
            'receiver_id'=>$request->receiver_id,
        ]);
        return new FriendRequestResource($friendRequest);
    }

    private function checkFriendExists(Request $request)
    {
        return $request->user()->send_friend_requests()->where('receiver_id',$request->receiver_id)
                ->exists() == 1
                || $request->user()->receive_friend_requests()->where('sender_id',$request->receiver_id)
                ->exists() == 1;
    }
    public function show(FriendRequest $request)
    {
        if ($request->sender_id == auth()->user()->id
            || $request->receiver_id == auth()->user()->id
            || auth()->user()->role == 'admin'){
                if ($request->receiver_id == auth()->user()->id){
                    $request->update(['is_read'=>true]);
                }
                return new FriendRequestResource($request);
        }
        return response()->json(['message'=>'your can\'t do that'],403);
    }

    public function destroy(FriendRequest $request)
    {
        if ($request->sender_id == auth()->user()->id
            || $request->receiver_id == auth()->user()->id
            || auth()->user()->role == 'admin'){
                $request->delete();
                return response()->noContent();
        }
        return response()->json(['message'=>'your can\'t do that'],403);
    }
}
