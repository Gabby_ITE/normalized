<?php

namespace App\Http\Controllers\Api\Request;

use App\Http\Controllers\Controller;
use App\Http\Resources\Request\ResponseResource;
use App\Models\Request\Response;
use Illuminate\Http\Request;

class ResponseController extends Controller
{
    public function index()
    {
        $sends = auth()->user()->send_responses;
        $receiver = auth()->user()->receive_responses;

        $fill = $sends->union($receiver)->sortBy('is_read')->sortByDesc('created_at');
        return ResponseResource::collection($fill);
    }

    public function show(Response $response)
    {
        if (auth()->user()->role == 'admin'
            || $response->sender_id == auth()->user()->id
            || $response->receiver_id == auth()->user()->id){
                if ($response->receiver_id == auth()->user()->id){
                    $response->update(['is_read'=>true]);
                }

                return new ResponseResource($response);
            }
        return response()->json(['message'=>'you can\'t do that']);
    }
}
