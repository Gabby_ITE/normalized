<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**public function via($notifiable)
    {
        return ['mail'];
    }
     * Create a new message instance.
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $validationCode
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.validation-code')
                    ->with([
                        'user' => $this->user,
                        'validationCode' => $this->user->validation_code,
                    ]);
    }
}
