<?php

use App\Models\Author\Author;
use App\Models\Author\AuthorBook;
use App\Models\Author\AuthorRating;
use App\Models\Author\AuthorSeries;
use App\Models\Author\FavoriteAuthor;
use App\Models\Book\Book;
use App\Models\Book\BookDescription;
use App\Models\Book\BookDiscussion;
use App\Models\Book\BookDiscussionReply;
use App\Models\Book\Bookmark;
use App\Models\Book\BookNote;
use App\Models\Book\BookRating;
use App\Models\Book\BookReview;
use App\Models\Book\BookReviewDisLike;
use App\Models\Book\BookReviewLike;
use App\Models\Book\BookStatus;
use App\Models\Challenge\Challenge;
use App\Models\Challenge\ChallengeProgress;
use App\Models\Club\Club;
use App\Models\Club\ClubMember;
use App\Models\Club\ClubPost;
use App\Models\Club\ClubPostReply;
use App\Models\Friend;
use App\Models\Logout;
use App\Models\Request\BookRequest;
use App\Models\Request\ClubRequest;
use App\Models\Request\FriendRequest;
use App\Models\Request\Report;
use App\Models\Request\Response;
use App\Models\Series\BookSeries;
use App\Models\Series\Series;
use App\Models\Series\SeriesDescription;
use App\Models\Series\SeriesDiscussion;
use App\Models\Series\SeriesDiscussionReply;
use App\Models\Series\SeriesRating;
use App\Models\Series\SeriesReview;
use App\Models\Series\SeriesReviewDisLike;
use App\Models\Series\SeriesReviewLike;
use App\Models\Tag\BookTag;
use App\Models\Tag\ClubTag;
use App\Models\Tag\FavoriteTag;
use App\Models\Tag\SeriesTag;
use App\Models\Tag\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

return [

    /*
     * All models in these directories will be scanned for ER diagram generation.
     * By default, the `app` directory will be scanned recursively for models.
     */
    'directories' => [
        base_path('app' . DIRECTORY_SEPARATOR . 'Models'),
    ],

    /*
     * If you want to ignore complete models or certain relations of a specific model,
     * you can specify them here.
     * To ignore a model completely, just add the fully qualified classname.
     * To ignore only a certain relation of a model, enter the classname as the key
     * and an array of relation names to ignore.
     */
    'ignore' => [
        // Author::class,
        // AuthorBook::class,
        // AuthorRating::class,
        // AuthorSeries::class,
        // FavoriteAuthor::class,

        // Book::class,
        // BookDescription::class,
        // BookDiscussion::class,
        // BookDiscussionReply::class,
        // Bookmark::class,
        // BookNote::class,
        // BookRating::class,
        // BookReview::class,
        // BookReviewLike::class,
        // BookReviewDisLike::class,
        // BookStatus::class,

        // Challenge::class,
        // ChallengeProgress::class,

        // Club::class,
        // ClubMember::class,
        // ClubPost::class,
        // ClubPostReply::class,

        // BookRequest::class,
        // ClubRequest::class,
        // FriendRequest::class,
        // Report::class,
        // Response::class,

        // Series::class,
        // SeriesDescription::class,
        // SeriesDiscussion::class,
        // SeriesDiscussionReply::class,
        // SeriesRating::class,
        // SeriesReview::class,
        // SeriesReviewLike::class,
        // SeriesReviewDisLike::class,
        // SeriesStatus::class,
        // BookSeries::class,


        // BookTag::class,
        // ClubTag::class,
        // FavoriteTag::class,
        // SeriesTag::class,
        // Tag::class,

        // Friend::class,
        // Logout::class
    ],

    /*
     * If you want to see only specific models, specify them here using fully qualified
     * classnames.
     *
     * Note: that if this array is filled, the 'ignore' array will not be used.
    */
    'whitelist' => [
        // App\User::class,
        // App\Post::class,
    ],

    /*
     * If true, all directories specified will be scanned recursively for models.
     * Set this to false if you prefer to explicitly define each directory that should
     * be scanned for models.
     */
    'recursive' => true,

    /*
     * The generator will automatically try to look up the model specific columns
     * and add them to the generated output. If you do not wish to use this
     * feature, you can disable it here.
     */
    'use_db_schema' => true,

    /*
     * This setting toggles weather the column types (VARCHAR, INT, TEXT, etc.)
     * should be visible on the generated diagram. This option requires
     * 'use_db_schema' to be set to true.
     */
    'use_column_types' => true,

    /*
     * These colors will be used in the table representation for each entity in
     * your graph.
     */
    'table' => [
        'header_background_color' => '#0000FF',
        'header_font_color' => '#000000',
        'row_background_color' => '#FFFBDC',
        'row_font_color' => '#000000',
    ],

    /*
     * Here you can define all the available Graphviz attributes that should be applied to your graph,
     * to its nodes and to the edge (the connection between the nodes). Depending on the size of
     * your diagram, different settings might produce better looking results for you.
     *
     * See http://www.graphviz.org/doc/info/attrs.html#d:label for a full list of attributes.
     */

     'graph' => [
        'style' => 'filled',
        'bgcolor' => '#F4F4F4',
        'fontsize' => 14,
        'labelloc' => 'c',
        'concentrate' => true,
        'splines' => 'ortho',
        'overlap' => true,
        'nodesep' => 1,
        'rankdir' => 'TB',
        'pad' => 1,
        'ranksep' => .5,
        'esep' => true,
        'fontname' => 'Helvetica Neue'
    ],

    'node' => [
        'margin' => 0,
        'shape' => 'rectangle',
        'fontname' => 'Helvetica Neue'
    ],

    'edge' => [
        'color' => '#000000',
        'penwidth' => 1.0,
        'fontname' => 'Helvetica Neue'
    ],

    'relations' => [
        // 'BelongsTo' => [],
        'HasMany' => [
            'dir' => 'both',
            // 'color' => '#F02A0C',
            'arrowhead' => 'crow',
            'arrowtail' => 'none',
        ],
    ],
];
