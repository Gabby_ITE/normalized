<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('series_review_dis_likes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('series_review_id')
                    ->constrained('series_reviews')
                    ->cascadeOnDelete();
            $table->foreignId('user_id')->constrained('users')
                    ->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('series_review_dis_likes');
    }
};
