<?php

namespace Database\Factories\Challenge;

use App\Models\Book\Book;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Challenge\Challenge>
 */
class ChallengeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $start = fake()->date();
        $end = fake()->date();
        if ($start>$end){
            $temp = $start;
            $start = $end;
            $end = $temp;
        }
        $type = fake()->randomElement(['books','pages']);
        $max_number = 0;
        if ($type == 'books'){
            $max_number = fake()->numberBetween(1,10);
            return [
                'creator_id'=>User::all()->random()->id,
                'name'=>fake()->name(),
                'type'=>$type,
                'is_private'=>fake()->boolean(),
                'max_number'=>$max_number,
                'start_date'=>$start,
                'end_date'=>$end,
            ];
        }else{
            $max_number = fake()->numberBetween(1,100);
            return [
                'creator_id'=>User::all()->random()->id,
                'book_id'=>Book::all()->random()->id,
                'name'=>fake()->title(),
                'is_private'=>fake()->boolean(),
                'type'=>$type,
                'max_number'=>$max_number,
                'start_date'=>$start,
                'end_date'=>$end,
            ];
        }
    }
}
