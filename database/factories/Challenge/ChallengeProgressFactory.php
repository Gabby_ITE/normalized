<?php

namespace Database\Factories\Challenge;

use App\Models\Challenge\Challenge;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Challenge\ChallengeProgress>
 */
class ChallengeProgressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $challenge = Challenge::inRandomOrder()->first();
        $user = User::inRandomOrder()->first();
        while($challenge != null && method_exists($challenge->progresses(),'exists')
                && $challenge->progresses()->get()->where('user_id',$user)->exists()){
            $challenge = Challenge::inRandomOrder()->first();
            $user = User::inRandomOrder()->first();
        }
        $acheived_number = fake()->numberBetween(1,$challenge->max_number);
        $stauts = $this->getStatus($challenge,$acheived_number,$challenge->max_number);
        return [
            'user_id'=>$user->id,
            'challenge_id'=>$challenge->id,
            'acheived_number'=>$acheived_number,
            'status' =>$stauts,
        ];
    }
    private function getStatus(Challenge $challenge,$acheived_number,$max_number){
        if (now() < $challenge->end_date){
            if ($acheived_number < $max_number)
                return 'not_decide';
            return 'finished';
        }
        return 'timeout';
    }

}
