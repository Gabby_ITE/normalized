<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Friend>
 */
class FriendFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $sender = User::all()->random();
        $receiver = User::all()->random();
        while ($sender->receive_friend_requests()->where('receiver_id',$receiver->id)->exists() == 1
            || $sender->send_friend_requests()->where('sender_id',$receiver->id)->exists() == 1
            || $sender == $receiver){
                $receiver = User::all()->random();
            }
        return [
            'sender_id'=>$sender,
            'receiver_id'=>$receiver
        ];
    }
}
