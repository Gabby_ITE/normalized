<?php

namespace Database\Factories\Book;

use App\Models\Book\Book;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book\BookReview>
 */
class BookReviewFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id'=>User::inRandomOrder()->first(),
            'book_id'=>Book::inRandomOrder()->first(),
            'text'=>fake()->text(200),
            'is_burn'=>fake()->boolean(),
        ];
    }
}
