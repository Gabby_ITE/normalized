<?php

namespace Database\Factories\Book;

use App\Models\Book\Book;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book\BookNote>
 */
class BookNoteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $book = Book::all()->random();
        return [
            'book_id'=>$book,
            'user_id'=>User::all()->random()->id,
            'page_number'=>fake()->numberBetween(1,$book->number_of_pages),
            'note'=>fake()->text(200),
            'color'=>fake()->colorName(),
        ];
    }
}
