<?php

namespace Database\Factories\Book;

use App\Models\Book\Book;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book\BookStatus>
 */
class BookStatusFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::inRandomOrder()->first();
        $book = Book::inRandomOrder()->first();
        while($book->statuses()->where('user_id',$user->id)->exists()){
            $user = User::inRandomOrder()->first();
            $book = Book::inRandomOrder()->first();
        }
        $status = fake()->randomElement(['reading','finished','watch_later']);
        $progress = 0;
        if ($status == 'finished'){
            $progress = $book->number_of_pages;
        }
        if ($status == 'reading'){
            $progress = fake()->numberBetween(0,$book->number_of_pages);
        }
        return [
            'user_id'=>$user->id,
            'book_id'=>$book->id,
            'status'=>$status,
            'progress'=>$progress,
        ];
    }
}
