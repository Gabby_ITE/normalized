<?php

namespace Database\Factories\Book;

use App\Models\Book\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book\BookDescription>
 */
class BookDescriptionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'text'=>fake()->text(200),
            'book_id'=>Book::all()->random()->id,
            'name'=>fake()->name(),
        ];
    }
}
