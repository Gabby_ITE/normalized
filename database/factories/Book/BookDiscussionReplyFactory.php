<?php

namespace Database\Factories\Book;

use App\Models\Book\BookDescription;
use App\Models\Book\BookDiscussion;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book\BookDiscussionReply>
 */
class BookDiscussionReplyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $bookDiscussion = BookDiscussion::all()->random();
        if (fake()->boolean()){
            // $reply = $bookDiscussion->replies->random();
            $replies = $bookDiscussion->replies;
            if ($replies->isNotEmpty() == 1){
                $reply = $replies->random();
                return [
                    'user_id'=>User::all()->random()->id,
                    'book_discussion_id'=>$bookDiscussion->id,
                    'book_discussion_reply_id'=>$reply->id,
                    'text'=>fake()->text(rand(10,50)),
                ];
            }
        }
        return [
            'user_id'=>User::all()->random()->id,
            'book_discussion_id'=>$bookDiscussion->id,
            'text'=>fake()->text(rand(10,50)),
        ];
    }
}
