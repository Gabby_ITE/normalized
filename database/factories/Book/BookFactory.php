<?php

namespace Database\Factories\Book;

use App\Models\Book\Book;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book\Book>
 */
class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

     protected $model = Book::class;

    public function definition(): array
    {
        $userId = 1;
        $is_private = 0;
        if (fake()->boolean){
            $userId =User::all()->random()->id;
            $is_private = 1;
        }
        return [
            'user_id'=>$userId,
            'title'=>fake()->title(),
            'cover'=>fake()->imageUrl($width = 400, $height = 600),
            'file'=>fake()->filePath(),
            'number_of_pages'=>fake()->numberBetween(1,2000),
            'release_date'=>fake()->date(),
            'is_private'=>$is_private,
        ];
    }
}
