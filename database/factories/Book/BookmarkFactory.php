<?php

namespace Database\Factories\Book;

use App\Models\Book\Book;
use App\Models\Book\Bookmark;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book\Bookmark>
 */
class BookmarkFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $book = Book::all()->random();
        $page_number = fake()->numberBetween(1,$book->number_of_pages);
        return [
            'book_id'=>Book::all()->random()->id,
            'user_id'=>User::all()->random()->id,
            'page_number'=>$page_number,
        ];
    }
}
