<?php

namespace Database\Factories\Book;

use App\Models\Book\BookReview;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book\BookReviewLike>
 */
class BookReviewLikeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'book_review_id'=>BookReview::all()->random()->id,
            'user_id'=>User::all()->random()->id,
        ];
    }
}
