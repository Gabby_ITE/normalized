<?php

namespace Database\Factories\Club;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Club\Club>
 */
class ClubFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $is_private = fake()->boolean();
        $is_closed = fake()->boolean();
        if ($is_private){
            $is_closed = $is_private;
        }
        return [
            'admin_id'=>User::all()->random()->id,
            'name'=>fake()->title(),
            'image'=>fake()->imageUrl(),
            'description'=>fake()->text(rand(50,100)),
            'is_private'=>$is_private,
            'is_closed'=>$is_closed,
        ];
    }
}
