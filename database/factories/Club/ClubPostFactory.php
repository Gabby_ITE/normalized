<?php

namespace Database\Factories\Club;

use App\Models\Club\ClubMember;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Club\ClubPost>
 */
class ClubPostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $member = ClubMember::all()->random();
        return [
            'club_id'=>$member->club_id,
            'user_id'=>$member->user_id,
            'text'=>fake()->text(),
        ];
    }
}
