<?php

namespace Database\Factories\Club;

use App\Models\Club\Club;
use App\Models\Club\ClubPost;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Club\ClubPostReply>
 */
class ClubPostReplyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $post = ClubPost::all()->random();
        $club = $post->club;
        $member = $club->members->random();
        if (fake()->boolean()){
            return [
                'user_id'=>$member->user_id,
                'club_post_id'=>$post->id,
                'text' => $this->faker->text(rand(10, 200)),
            ];
        }else{
            $replies = $post->replies;
            if ($replies->isNotEmpty() == 1){
                $reply = $replies->random();
                return [
                    'user_id'=>$member->user_id,
                    'club_post_id'=>$post->id,
                    'club_post_reply_id'=>$reply->id,
                    'text' => $this->faker->text(rand(10, 200)),
                ];
            }else{
                return [
                    'user_id'=>$member->user_id,
                    'club_post_id'=>$post->id,
                    'text' => $this->faker->text(rand(10, 200)),
                ];
            }
        }
    }
}
