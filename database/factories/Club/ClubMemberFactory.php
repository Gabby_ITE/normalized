<?php

namespace Database\Factories\Club;

use App\Models\Club\Club;
use App\Models\Club\ClubMember;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Club\ClubMember>
 */
class ClubMemberFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $clubId = Club::all()->random()->id;
        $userId = User::all()->random()->id;
        while(ClubMember::where('club_id',$clubId)->where('user_id',$userId)->exists()==1){
            $clubId = Club::all()->random()->id;
            $userId = User::all()->random()->id;
        }
        return [
            'user_id'=>$userId,
            'club_id'=>$clubId,
        ];
    }
}
