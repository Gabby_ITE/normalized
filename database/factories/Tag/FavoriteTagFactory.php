<?php

namespace Database\Factories\Tag;

use App\Models\Tag\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tag\FavoriteTag>
 */
class FavoriteTagFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tag = Tag::inRandomOrder()->first();
        $user = User::inRandomOrder()->first();

        while ($user->tags()->where('tag_id', $tag->id)->exists() == 1) {
            $tag = Tag::inRandomOrder()->first();
            $user = User::inRandomOrder()->first();
        }

        return [
            'tag_id'=>$tag->id,
            'user_id'=>$user->id,
        ];
    }
}
