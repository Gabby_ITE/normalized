<?php

namespace Database\Factories\Tag;

use App\Models\Series\Series;
use App\Models\Tag\Tag;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tag\SeriesTag>
 */
class SeriesTagFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tag = Tag::inRandomOrder()->first();
        $series = Series::inRandomOrder()->first();

        while ($series->tags()->where('tag_id', $tag->id)->exists()) {
            $tag = Tag::inRandomOrder()->first();
            $series = Series::inRandomOrder()->first();
        }

        return [
            'tag_id'=>$tag->id,
            'series_id'=>$series->id,
        ];
    }
}
