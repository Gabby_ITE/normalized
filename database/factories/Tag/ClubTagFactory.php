<?php

namespace Database\Factories\Tag;

use App\Models\Club\Club;
use App\Models\Tag\ClubTag;
use App\Models\Tag\Tag;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tag\ClubTag>
 */
class ClubTagFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $clubId = Club::all()->random()->id;
        $tagId = Tag::all()->random()->id;
        while(ClubTag::where('club_id',$clubId)->where('tag_id',$tagId)->exists() == 1){
            $clubId = Club::all()->random()->id;
            $tagId = Tag::all()->random()->id;
        }
        return [
            'club_id'=>$clubId,
            'tag_id'=>$tagId,
        ];
    }
}
