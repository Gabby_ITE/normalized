<?php

namespace Database\Factories\Tag;

use App\Models\Book\Book;
use App\Models\Tag\Tag;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tag\BookTag>
 */
class BookTagFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tag = Tag::inRandomOrder()->first();
        $book = Book::inRandomOrder()->first();

        while ($book->tags()->where('tag_id', $tag->id)->exists()) {
            $tag = Tag::inRandomOrder()->first();
            $book = Book::inRandomOrder()->first();
        }

        return [
            'tag_id'=>$tag->id,
            'book_id'=>$book->id,
        ];
    }
}
