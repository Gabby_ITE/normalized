<?php

namespace Database\Factories\Author;

use App\Models\Author\Author;
use App\Models\Author\FavoriteAuthor;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Author\FavoriteAuthor>
 */
class FavoriteAuthorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $author = Author::all()->random();
        $user = User::all()->random();
        while($user->authors()->where('author_id',$author->id)->exists() == 1){
            $author = Author::all()->random();
            $user = User::all()->random();
        }
        return [
            'user_id'=>$user->id,
            'author_id'=>$author->id,
        ];
    }
}
