<?php

namespace Database\Factories\Author;

use App\Models\Author\Author;
use App\Models\Author\AuthorBook;
use App\Models\Book\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

class AuthorBookFactory extends Factory
{
 protected $model = AuthorBook::class;

 public function definition()
 {
    $author = Author::all()->random();
    $book = Book::all()->random();

    while($author->books()->where('book_id',$book->id)->exists()==1){
        $author = Author::all()->random();
        $book = Book::all()->random();
    }

    return [
        'author_id' => $author->id,
        'book_id' => $book->id,
    ];
 }
}
