<?php

namespace Database\Factories\Author;

use App\Models\Author\Author;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Author\AuthorRating>
 */
class AuthorRatingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::inRandomOrder()->first();
        $author = Author::inRandomOrder()->first();

        while ($author->ratings()->where('user_id', $user->id)->exists()) {
            $user = User::inRandomOrder()->first();
            $author = Author::inRandomOrder()->first();
        }

        return [
            'user_id'=>$user->id,
            'author_id'=>$author->id,
            'rating'=>fake()->numberBetween(1,5),
        ];
    }
}
