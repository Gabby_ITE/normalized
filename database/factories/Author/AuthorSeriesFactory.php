<?php

namespace Database\Factories\Author;

use App\Models\Author\Author;
use App\Models\Author\AuthorBook;
use App\Models\Author\AuthorSeries;
use App\Models\Series\Series;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Series\Series>
 */
class AuthorSeriesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $authors = Author::inRandomOrder()->get();
        $serieses = Series::inRandomOrder()->get();
        $authorSeries = AuthorSeries::all();
        foreach ($authors as $author) {
            foreach ($serieses as $series) {
                $asExits = false;
                foreach($authorSeries as $aS){
                    if ($aS->author_id == $author->id && $aS->series_id == $series->id) {
                        $asExits = true;
                        break;
                    }
                }
                foreach ($series->books->pluck('book') as $book){
                    $exists = false;
                    foreach($book->authors as $authorBook){
                        if ($authorBook->author_id == $author->id)
                            $exists = true;
                    }
                    if (!$exists){
                        AuthorBook::create([
                            'author_id'=> $author->id,
                            'book_id' => $book->id
                        ]);
                    }
                }
                if (!$asExits){
                    return [
                        'author_id' => $author->id,
                        'series_id' => $series->id,
                    ];
                }
            }

        }
    }
}
