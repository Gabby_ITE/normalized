<?php

namespace Database\Factories\Series;

use App\Models\Series\SeriesReview;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Series\SeriesReviewLike>
 */
class SeriesReviewLikeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'series_review_id'=>SeriesReview::all()->random()->id,
            'user_id'=>User::all()->random()->id,
        ];
    }
}
