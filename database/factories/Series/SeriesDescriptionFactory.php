<?php

namespace Database\Factories\Series;

use App\Models\Series\Series;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Series\SeriesDescription>
 */
class SeriesDescriptionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'text'=>fake()->text(200),
            'series_id'=>Series::all()->random()->id,
            'name'=>fake()->name(),
        ];
    }
}
