<?php

namespace Database\Factories\Series;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Series\Series>
 */
class SeriesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title'=>fake()->title(),
            'cover'=>fake()->imageUrl($width = 400, $height = 400),
            'release_date'=>fake()->date(),
        ];
    }
}
