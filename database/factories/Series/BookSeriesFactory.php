<?php
namespace Database\Factories\Series;

use App\Models\Book\Book;
use App\Models\Series\Series;
use App\Models\Series\BookSeries;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookSeriesFactory extends Factory
{
    protected $model = BookSeries::class;

    public function definition(): array
    {
        $series = Series::inRandomOrder()->first();
        $book = Book::inRandomOrder()->first();

        while ($series->books()->where('book_id', $book->id)->exists()) {
            $series = Series::inRandomOrder()->first();
            $book = Book::inRandomOrder()->first();
        }

        return [
            'series_id' => $series->id,
            'book_id' => $book->id,
        ];
    }
}
