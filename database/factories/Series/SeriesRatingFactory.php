<?php

namespace Database\Factories\Series;

use App\Models\Series\Series;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Series\SeriesRating>
 */
class SeriesRatingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::inRandomOrder()->first();
        $series = Series::inRandomOrder()->first();

        while ($series->ratings()->where('user_id', $user->id)->exists()) {
            $user = User::inRandomOrder()->first();
            $series = Series::inRandomOrder()->first();
        }

        return [
            'user_id'=>$user->id,
            'series_id'=>$series->id,
            'rating'=>fake()->numberBetween(1,5),
        ];
    }
}
