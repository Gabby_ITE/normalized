<?php

namespace Database\Factories\Series;

use App\Models\Series\Series;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Series\SeriesReview>
 */
class SeriesReviewFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id'=>User::inRandomOrder()->first(),
            'series_id'=>Series::inRandomOrder()->first(),
            'text'=>fake()->text(200),
            'is_burn'=>fake()->boolean(),
        ];
    }
}
