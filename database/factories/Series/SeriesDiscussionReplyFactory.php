<?php

namespace Database\Factories\Series;

use App\Models\Series\SeriesDiscussion;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Series\SeriesDiscussionReply>
 */
class SeriesDiscussionReplyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $seriesDiscussion = SeriesDiscussion::all()->random();
        if (fake()->boolean()){
            $replies = $seriesDiscussion->replies;
            if ($replies->isNotEmpty() == 1){
                $reply = $replies->random();
                return [
                    'user_id'=>User::all()->random()->id,
                    'series_discussion_id'=>$seriesDiscussion->id,
                    'series_discussion_reply_id'=>$reply->id,
                    'text'=>fake()->text(rand(10,50)),
                ];
            }
        }
        return [
            'user_id'=>User::all()->random()->id,
            'series_discussion_id'=>$seriesDiscussion->id,
            'text'=>fake()->text(rand(10,50)),
        ];
    }
}
