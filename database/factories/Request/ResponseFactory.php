<?php

namespace Database\Factories\Request;

use App\Models\Club\ClubMember;
use App\Models\Friend;
use App\Models\Request\BookRequest;
use App\Models\Request\ClubRequest;
use App\Models\Request\FriendRequest;
use App\Models\Request\Report;
use App\Models\Request\Response;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Request\Response>
 */
class ResponseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $request_type = fake()->randomElement(['friend','club','report','book']);
        $request = null;
        $type = fake()->randomElement(['accept','reject']);
        if ($request_type == 'friend'){
            $request = FriendRequest::all()->random();
        }
        if ($request_type == 'club'){
            $request = ClubRequest::all()->random();
        }
        if ($request_type == 'report'){
            $request = Report::all()->random();
        }
        else{
            $request = BookRequest::all()->random();
        }
        while(Response::where('request_type',$request_type)->where('request_id',$request->id)->exists() == 1){
            if ($request_type == 'friend'){
                $request = FriendRequest::all()->random();
            }
            if ($request_type == 'club'){
                $request = ClubRequest::all()->random();
            }
            if ($request_type == 'report'){
                $request = Report::all()->random();
            }
            else{
                $request = BookRequest::all()->random();
            }
        }

        if ($request_type == 'friend'){
            $request = FriendRequest::all()->random();
            if ($type == 'accept'){
                Friend::create([
                    'sender_id'=>$request->sender_id,
                    'receiver_id'=>$request->receiver_id,
                ]);
            }
            $request->update(['is_read'=>false]);
            $request->update(['is_done'=>true]);
        }
        if ($request_type == 'club'){
            $request = ClubRequest::all()->random();
            if ($type == 'accept'){
                ClubMember::create([
                    'user_id'=>$request->sender_id,
                    'club_id'=>$request->club_id,
                ]);
            }
            $request->update(['is_read'=>false]);
            $request->update(['is_done'=>true]);
        }
        if ($request_type == 'report'){
            $request = Report::all()->random();
            if ($type == 'accept'){
                $request->delete();
            }
            $request->update(['is_read'=>false]);
            $request->update(['is_done'=>true]);
        }
        else{
            $request = BookRequest::all()->random();
            $request->update(['is_done'=>true]);
            $request->update(['is_read'=>false]);
        }

        return [
            'sender_id'=>$request->receiver_id,
            'receiver_id'=>$request->sender_id,
            'type'=>$type,
            'request_type'=>$request_type,
            'request_id'=>$request->id,
        ];
    }
}
