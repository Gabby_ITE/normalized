<?php

namespace Database\Factories\Request;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Request\BookRequest>
 */
class BookRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $sender = User::all()->random();
        return [
            'sender_id'=>$sender->id,
            'receiver_id'=>1,
            'title'=>fake()->title(),
            'author_name'=>fake()->name(),
        ];
    }
}
