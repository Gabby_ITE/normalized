<?php

namespace Database\Factories\Request;

use App\Models\Club\Club;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Request\ClubRequest>
 */
class ClubRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $club = Club::all()->random();
        $sender = User::all()->random();
        while($sender->send_club_requests()->where('club_id',$club->id)->exists() == 1
            && $sender->send_club_requests()->count() < Club::all()->count()){
            $club = Club::all()->random();
        }
        return [
            'sender_id'=>$sender->id,
            'receiver_id'=>$club->admin_id,
            'club_id'=>$club->id,
        ];
    }
}
