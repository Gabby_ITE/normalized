<?php

namespace Database\Factories\Request;

use App\Models\Book\BookDiscussion;
use App\Models\Book\BookDiscussionReply;
use App\Models\Book\BookReview;
use App\Models\Club\ClubPost;
use App\Models\Club\ClubPostReply;
use App\Models\Series\SeriesDiscussion;
use App\Models\Series\SeriesDiscussionReply;
use App\Models\Series\SeriesReview;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Request\Report>
 */
class ReportFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $reported_type = fake()->randomElement([
            'series_review',
            'book_review',
            'book_discussion',
            'book_discussion_reply',
            'series_discussion',
            'series_discussion_reply',
            'club_post',
            'club_post_reply'
        ]);

        $reported_id = 1;

        if ($reported_type == 'series_review'){
            $reported_id = SeriesReview::all()->random()->id;
        }
        if ($reported_type == 'book_review'){
            $reported_id = BookReview::all()->random()->id;
        }
        if ($reported_type == 'book_discussion'){
            $reported_id = BookDiscussion::all()->random()->id;
        }
        if ($reported_type == 'book_discussion_reply'){
            $reported_id = BookDiscussionReply::all()->random()->id;
        }

        if ($reported_type == 'series_discussion'){
            $reported_id = SeriesDiscussion::all()->random()->id;
        }
        if ($reported_type == 'series_discussion_reply'){
            $reported_id = SeriesDiscussionReply::all()->random()->id;
        }
        if ($reported_type == 'club_post'){
            $reported_id = ClubPost::all()->random()->id;
        }
        if ($reported_type == 'club_post_reply'){
            $reported_id = ClubPostReply::all()->random()->id;
        }

        return [
            'sender_id'=>User::all()->random()->id,
            'receiver_id'=>1,
            'message'=>fake()->text(rand(10,100)),
            'reported_type'=>$reported_type,
            'reported_id'=>$reported_id,
        ];
    }
}
