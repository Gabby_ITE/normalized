<?php

namespace Database\Factories\Request;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Request\FriendRequest>
 */
class FriendRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $sender = User::all()->random();
        $receiver = User::all()->random();
        while($sender->send_friend_requests()->where('receiver_id',$receiver->id)->exists() == 1
            || $sender->receive_friend_requests()->where('sender_id',$receiver->id)->exists() == 1
            && $sender->send_friend_requests()->count() + $sender->receive_friend_requests()->count() < User::all()->count()){
                $sender = User::all()->random();
                $receiver = User::all()->random();
            }
        return [
            'sender_id'=>$sender->id,
            'receiver_id'=>$receiver->id,
        ];
    }
}
