<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Author\Author;
use App\Models\Author\AuthorBook;
use App\Models\Author\AuthorRating;
use App\Models\Author\AuthorSeries;
use App\Models\Author\FavoriteAuthor;
use App\Models\Book\Book;
use App\Models\Book\BookDescription;
use App\Models\Book\BookDiscussion;
use App\Models\Book\BookDiscussionReply;
use App\Models\Book\BookHighLight;
use App\Models\Book\Bookmark;
use App\Models\Book\BookNote;
use App\Models\Book\BookRating;
use App\Models\Book\BookReview;
use App\Models\Book\BookReviewDisLike;
use App\Models\Book\BookReviewLike;
use App\Models\Book\BookStatus;
use App\Models\Challenge\Challenge;
use App\Models\Challenge\ChallengeProgress;
use App\Models\Club\Club;
use App\Models\Club\ClubMember;
use App\Models\Club\ClubMessage;
use App\Models\Club\ClubPost;
use App\Models\Club\ClubPostReply;
use App\Models\Friend;
use App\Models\Logout;
use App\Models\Request\BookRequest;
use App\Models\Request\ClubRequest;
use App\Models\Request\FriendRequest;
use App\Models\Request\Report;
use App\Models\Request\Response;
use App\Models\Series\BookSeries;
use App\Models\Series\Series;
use App\Models\Series\SeriesDescription;
use App\Models\Series\SeriesDiscussion;
use App\Models\Series\SeriesDiscussionReply;
use App\Models\Series\SeriesRating;
use App\Models\Series\SeriesReview;
use App\Models\Series\SeriesReviewDisLike;
use App\Models\Series\SeriesReviewLike;
use App\Models\Tag\BookTag;
use App\Models\Tag\ClubTag;
use App\Models\Tag\FavoriteTag;
use App\Models\Tag\SeriesTag;
use App\Models\Tag\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::create([
            'first_name'=>'gabby',
            'last_name'=>'hanna',
            'email'=>'gabbyalhanna.cs@gmail.com',
            'role'=>'admin',
            'password'=>Hash::make('hello world'),
        ]);
        User::factory(1000)->create();
        Book::factory(100)->create();
        Tag::factory(100)->create();
        Author::factory(10)->create();
        Series::factory(10)->create();

        AuthorBook::factory(70)->create();
        AuthorRating::factory(30)->create();
        AuthorSeries::factory(10)->create();

        BookDescription::factory(100)->create();
        BookDiscussion::factory(200)->create();
        BookRating::factory(200)->create();
        BookReview::factory(30)->create();
        BookReviewDisLike::factory(10)->create();
        BookReviewLike::factory(10)->create();
        BookStatus::factory(30)->create();

        Challenge::factory(20)->create();
        ChallengeProgress::factory(20)->create();

        BookSeries::factory(30)->create();
        SeriesDescription::factory(10)->create();
        SeriesDiscussion::factory(10)->create();
        SeriesRating::factory(10)->create();
        SeriesReview::factory(10)->create();
        SeriesReviewDisLike::factory(20)->create();
        SeriesReviewLike::factory(10)->create();

        FavoriteTag::factory(30)->create();
        FavoriteAuthor::factory(30)->create();

        Club::factory(100)->create();
        ClubMember::factory(20)->create();

        ClubTag::factory(20)->create();
        BookTag::factory(200)->create();
        SeriesTag::factory(30)->create();

        BookDiscussionReply::factory(100)->create();
        SeriesDiscussionReply::factory(100)->create();

        ClubPost::factory(50)->create();
        ClubPostReply::factory(100)->create();

        Friend::factory(100)->create();

        FriendRequest::factory(400)->create();
        ClubRequest::factory(400)->create();
        BookRequest::factory(30)->create();
        Report::factory(30)->create();

        Response::factory(100)->create();

        Bookmark::factory(100)->create();
        BookNote::factory(100)->create();
        Logout::factory(100)->create();
    }
}
