<?php

use App\Http\Controllers\Api\Author\AuthorBookController;
use App\Http\Controllers\Api\Author\AuthorController;
use App\Http\Controllers\Api\Author\AuthorRatingController;
use App\Http\Controllers\Api\Author\AuthorSeriesController;
use App\Http\Controllers\Api\Author\FavoriteAuthorController;
use App\Http\Controllers\Api\User\UserController;
use App\Http\Controllers\Api\Book\BookController;
use App\Http\Controllers\Api\Book\BookDescriptionController;
use App\Http\Controllers\Api\Book\BookDiscussionController;
use App\Http\Controllers\Api\Book\BookDiscussionReplyController;
use App\Http\Controllers\Api\Book\BookmarksController;
use App\Http\Controllers\Api\Book\BookNoteController;
use App\Http\Controllers\Api\Book\BookRatingController;
use App\Http\Controllers\Api\Book\BookReviewContorller;
use App\Http\Controllers\Api\Book\BookReviewDisLikeController;
use App\Http\Controllers\Api\Book\BookReviewLikeController;
use App\Http\Controllers\Api\Book\BookStatusController;
use App\Http\Controllers\Api\Challenge\ChallengeController;
use App\Http\Controllers\Api\Challenge\ChallengeProgressController;
use App\Http\Controllers\Api\Challenge\UserChallengeController;
use App\Http\Controllers\Api\Club\ClubController;
use App\Http\Controllers\Api\Club\ClubMemberController;
use App\Http\Controllers\Api\Club\ClubPostController;
use App\Http\Controllers\Api\Club\ClubPostReplyController;
use App\Http\Controllers\Api\Request\BookRequestController;
use App\Http\Controllers\Api\Request\ClubRequestController;
use App\Http\Controllers\Api\Request\FriendRequestController;
use App\Http\Controllers\Api\Request\ReportController;
use App\Http\Controllers\Api\Request\RequestHandlerController;
use App\Http\Controllers\Api\Request\ResponseController;
use App\Http\Controllers\Api\Search\FilterController;
use App\Http\Controllers\Api\Search\ForYouController;
use App\Http\Controllers\Api\Search\FriendFeedsController;
use App\Http\Controllers\Api\Search\SearchController;
use App\Http\Controllers\Api\Series\BookSeriesController;
use App\Http\Controllers\Api\Series\SeriesController;
use App\Http\Controllers\Api\Series\SeriesDescriptionController;
use App\Http\Controllers\Api\Series\SeriesDiscussionController;
use App\Http\Controllers\Api\Series\SeriesDiscussionReplyController;
use App\Http\Controllers\Api\Series\SeriesRatingController;
use App\Http\Controllers\Api\Series\SeriesReviewController;
use App\Http\Controllers\Api\Series\SeriesReviewDisLikeController;
use App\Http\Controllers\Api\Series\SeriesReviewLikeController;
use App\Http\Controllers\Api\Tag\BookTagController;
use App\Http\Controllers\Api\Tag\ClubTagController;
use App\Http\Controllers\Api\Tag\FavoriteTagController;
use App\Http\Controllers\Api\Tag\SeriesTagController;
use App\Http\Controllers\Api\Tag\TagController;
use App\Http\Controllers\Api\User\AuthController;
use App\Http\Controllers\Api\User\FriendController;
use App\Models\Author\Author;
use App\Models\Author\AuthorSeries;
use App\Models\Book\Book;
use App\Models\Book\Bookmark;
use App\Models\Series\Series;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Routing\RequestContext;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function () {
    Route::middleware(['guest'])->group(function () {
        Route::post('/register',[AuthController::class,'register']);
        Route::post('/register/validate', [AuthController::class,'validateRegistration']);
        Route::post('/login',[AuthController::class,'login']);
    });
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::post('/logout',[AuthController::class,'logout']);
        Route::get('/reset-password',[AuthController::class,'resetPassword']);
        Route::post('/reset-password/validate',[AuthController::class,'validateResetPassword']);
    });
});
Route::middleware(['auth:sanctum'])->group(function () {
    Route::apiResource('users',UserController::class);
    Route::apiResource('authors', AuthorController::class);
    Route::apiResource('books', BookController::class);
    Route::apiResource('series',SeriesController::class);
    Route::apiResource('tags',TagController::class);
    Route::apiResource('challenges',ChallengeController::class);
    Route::apiResource('clubs',ClubController::class);

    Route::post('users/{user}/updateImage',[UserController::class,'updateImage']);
    Route::post('authors/{author}/updateImage',[AuthorController::class,'updateImage']);
    Route::post('books/{book}/updateImage',[BookController::class,'updateImage']);
    Route::post('series/{series}/updateImage',[SeriesController::class,'updateImage']);
    Route::post('clubs/{club}/updateImage',[ClubController::class,'updateImage']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('authors/{author}/series',[AuthorSeriesController::class,'series']);
    Route::get('authors/{author}/books',[AuthorBookController::class,'books']);
    Route::middleware(['admin'])->group(function () {
        Route::post('authors/{author}/books',[AuthorBookController::class,'addBook']);
        Route::delete('authors/{author}/books/{book}',[AuthorBookController::class,'deleteBook']);

        Route::post('authors/{author}/series',[AuthorSeriesController::class,'addSeries']);
        Route::delete('authors/{author}/series/{series}',[AuthorSeriesController::class,'deleteSeries']);
    });

});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/series/{series}/books',[BookSeriesController::class,'books']);
    Route::post('/series/{series}/books',[BookSeriesController::class,'addBook'])->middleware('admin');
    Route::delete('series/{series}/books/{bookSeries}',[BookSeriesController::class,'deleteBook'])->middleware('admin');;
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/books/{book}/suggests',[BookController::class,'suggests']);

    Route::get('/books/{book}/ratings',[BookRatingController::class,'index'])->middleware('admin');
    Route::post('/books/{book}/ratings',[BookRatingController::class,'store']);
    Route::delete('/books/{book}/ratings',[BookRatingController::class,'destroy']);
    Route::put('/books/{book}/ratings',[BookRatingController::class,'update']);
    Route::patch('/books/{book}/ratings',[BookRatingController::class,'update']);

    Route::get('/series/{series}/ratings',[SeriesRatingController::class,'index'])->middleware('admin');
    Route::post('/series/{series}/ratings',[SeriesRatingController::class,'store']);
    Route::delete('/series/{series}/ratings',[SeriesRatingController::class,'destroy']);
    Route::put('/series/{series}/ratings',[SeriesRatingController::class,'update']);
    Route::patch('/series/{series}/ratings',[SeriesRatingController::class,'update']);

    Route::get('/authors/{author}/ratings',[AuthorRatingController::class,'index'])->middleware('admin');
    Route::post('/authors/{author}/ratings',[AuthorRatingController::class,'store']);
    Route::delete('/authors/{author}/ratings',[AuthorRatingController::class,'destroy']);
    Route::put('/authors/{author}/ratings',[AuthorRatingController::class,'update']);
    Route::patch('/authors/{author}/ratings',[AuthorRatingController::class,'update']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('books/{book}/statuses',[BookStatusController::class,'index'])->middleware('admin');
    Route::post('books/{book}/statuses',[BookStatusController::class,'store']);
    Route::put('books/{book}/statuses',[BookStatusController::class,'update']);
    Route::delete('books/{book}/statuses',[BookStatusController::class,'destory']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('books/{book}/tags',[BookTagController::class,'index']);
    Route::get('series/{series}/tags',[SeriesTagController::class,'index']);
    Route::get('clubs/{club}/tags',[ClubTagController::class,'index']);

    Route::middleware(['admin'])->group(function () {
        Route::post('books/{book}/tags',[BookTagController::class,'store']);
        Route::post('books/{book}/tags/storeAll',[BookTagController::class,'storeAll']);
        Route::delete('books/{book}/tags/{tag}',[BookTagController::class,'destroy']);

        Route::post('series/{series}/tags',[SeriesTagController::class,'store']);
        Route::post('series/{series}/tags/storeAll',[SeriesTagController::class,'storeAll']);
        Route::delete('series/{series}/tags/{tag}',[SeriesTagController::class,'destroy']);
    });

    Route::post('clubs/{club}/tags',[ClubTagController::class,'store']);
    Route::post('clubs/{club}/tags/storeAll',[ClubTagController::class,'storeAll']);
    Route::delete('clubs/{club}/tags/{tag}',[ClubTagController::class,'destroy']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('books/{book}/reviews',[BookReviewContorller::class,'index']);
    Route::get('books/{book}/reviews/{review}',[BookReviewContorller::class,'show']);
    Route::post('books/{book}/reviews',[BookReviewContorller::class,'store']);
    Route::put('books/{book}/reviews/{review}',[BookReviewContorller::class,'update']);
    Route::delete('books/{book}/reviews/{review}',[BookReviewContorller::class,'destroy']);

    Route::get('series/{series}/reviews',[SeriesReviewController::class,'index']);
    Route::get('series/{series}/reviews/{review}',[SeriesReviewController::class,'show']);
    Route::post('series/{series}/reviews',[SeriesReviewController::class,'store']);
    Route::put('series/{series}/reviews/{review}',[SeriesReviewController::class,'update']);
    Route::delete('series/{series}/reviews/{review}',[SeriesReviewController::class,'destroy']);
});

Route::middleware(['auth:sanctum'])->group(function(){
    Route::get('books/{book}/reviews/{review}/likes',[BookReviewLikeController::class,'index']);
    Route::post('books/{book}/reviews/{review}/add-like',[BookReviewLikeController::class,'addLike']);
    Route::delete('books/{book}/reviews/{review}/delete-like',[BookReviewLikeController::class,'deleteLike']);

    Route::get('books/{book}/reviews/{review}/dis-likes',[BookReviewDisLikeController::class,'index']);
    Route::post('books/{book}/reviews/{review}/add-dis-like',[BookReviewDisLikeController::class,'addDisLike']);
    Route::delete('books/{book}/reviews/{review}/delete-dis-like',[BookReviewDisLikeController::class,'deleteDisLike']);

    Route::get('series/{series}/reviews/{review}/likes',[SeriesReviewLikeController::class,'index']);
    Route::post('series/{series}/reviews/{review}/add-like',[SeriesReviewLikeController::class,'addLike']);
    Route::delete('series/{series}/reviews/{review}/delete-like',[SeriesReviewLikeController::class,'deleteLike']);

    Route::get('series/{series}/reviews/{review}/dis-likes',[BookReviewDisLikeController::class,'index']);
    Route::post('series/{series}/reviews/{review}/add-dis-like',[SeriesReviewDisLikeController::class,'addDisLike']);
    Route::delete('series/{series}/reviews/{review}/delete-dis-like',[SeriesReviewDisLikeController::class,'deleteDisLike']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('books/{book}/discussions',[BookDiscussionController::class,'index']);
    Route::get('books/{book}/discussions/{discussion}',[BookDiscussionController::class,'show']);
    Route::post('books/{book}/discussions',[BookDiscussionController::class,'store']);
    Route::put('books/{book}/discussions/{discussion}',[BookDiscussionController::class,'update']);
    Route::delete('books/{book}/discussions/{discussion}',[BookDiscussionController::class,'destroy']);

    Route::post('books/{book}/discussions/{discussion}/replies',[BookDiscussionReplyController::class,'store']);
    Route::post('books/{book}/discussions/{discussion}/replies/{reply}',[BookDiscussionReplyController::class,'store_to_reply']);
    Route::get('books/{book}/discussions/{discussion}/replies/{reply}',[BookDiscussionReplyController::class,'show']);
    Route::put('books/{book}/discussions/{discussion}/replies/{reply}',[BookDiscussionReplyController::class,'update']);
    Route::delete('books/{book}/discussions/{discussion}/replies/{reply}',[BookDiscussionReplyController::class,'destroy']);

    Route::get('series/{series}/discussions',[SeriesDiscussionController::class,'index']);
    Route::get('series/{series}/discussions/{discussion}',[SeriesDiscussionController::class,'show']);
    Route::post('series/{series}/discussions',[SeriesDiscussionController::class,'store']);
    Route::put('series/{series}/discussions/{discussion}',[SeriesDiscussionController::class,'update']);
    Route::delete('series/{series}/discussions/{discussion}',[SeriesDiscussionController::class,'destroy']);

    Route::post('series/{series}/discussions/{discussion}/replies',[SeriesDiscussionReplyController::class,'store']);
    Route::post('series/{series}/discussions/{discussion}/replies/{reply}',[SeriesDiscussionReplyController::class,'store_to_reply']);
    Route::get('series/{series}/discussions/{discussion}/replies/{reply}',[SeriesDiscussionReplyController::class,'show']);
    Route::put('series/{series}/discussions/{discussion}/replies/{reply}',[SeriesDiscussionReplyController::class,'update']);
    Route::delete('series/{series}/discussions/{discussion}/replies/{reply}',[SeriesDiscussionReplyController::class,'destroy']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('favorites/tags',[FavoriteTagController::class,'index']);
    Route::post('favorites/tags',[FavoriteTagController::class,'store']);
    Route::post('favorites/tags/storeAll',[FavoriteTagController::class,'storeAll']);
    Route::delete('favorites/tags/{tag}',[FavoriteTagController::class,'destroy']);

    Route::get('favorites/authors',[FavoriteAuthorController::class,'index']);
    Route::post('favorites/authors',[FavoriteAuthorController::class,'store']);
    Route::post('favorites/authors/storeAll',[FavoriteAuthorController::class,'storeAll']);
    Route::delete('favorites/authors/{author}',[FavoriteAuthorController::class,'destroy']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('books/{book}/descriptions',[BookDescriptionController::class,'index']);
    Route::get('books/{book}/descriptions/{description}',[BookDescriptionController::class,'show']);

    Route::get('series/{series}/descriptions',[SeriesDescriptionController::class,'index']);
    Route::get('series/{series}/descriptions/{description}',[SeriesDescriptionController::class,'show']);

    Route::middleware(['admin'])->group(function () {
        Route::post('books/{book}/descriptions',[BookDescriptionController::class,'store']);
        Route::put('books/{book}/descriptions/{description}',[BookDescriptionController::class,'update']);
        Route::delete('books/{book}/descriptions/{description}',[BookDescriptionController::class,'destroy']);

        Route::post('series/{series}/descriptions',[SeriesDescriptionController::class,'store']);
        Route::put('series/{series}/descriptions/{description}',[SeriesDescriptionController::class,'update']);
        Route::delete('series/{series}/descriptions/{description}',[SeriesDescriptionController::class,'destroy']);
    });

});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('challenges/{challenge}/progresses',[ChallengeProgressController::class,'index']);
    Route::post('challenges/{challenge}/progresses',[ChallengeProgressController::class,'store']);
    Route::put('challenges/{challenge}/progresses',[ChallengeProgressController::class,'update']);
    Route::delete('challenges/{challenge}/progresses',[ChallengeProgressController::class,'delete']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('created/challenges',[UserChallengeController::class,'created_challenges']);
    Route::get('challenges/progressed/{status}',[UserChallengeController::class,'progressed_challenges']);
    Route::post('hello/{book}',[UserChallengeController::class,'chall']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('clubs/{club}/members',[ClubMemberController::class,'index']);
    Route::post('clubs/{club}/members',[ClubMemberController::class,'add']);
    Route::delete('clubs/{club}/members/{userId}',[ClubMemberController::class,'delete']);
});
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('joined_clubs',[ClubMemberController::class,'joined_clubs']);
    Route::get('joined_clubs/friends',[ClubMemberController::class,'friends_joined_clubs']);
    Route::get('clubs/{club}/members',[ClubMemberController::class,'index']);
    Route::post('clubs/{club}/members',[ClubMemberController::class,'add']);
    Route::delete('clubs/{club}/members/{userId}',[ClubMemberController::class,'delete']);

    Route::get('clubs/{club}/posts',[ClubPostController::class,'index']);
    Route::post('clubs/{club}/posts',[ClubPostController::class,'store']);
    Route::get('clubs/{club}/posts/{post}',[ClubPostController::class,'show']);
    Route::put('clubs/{club}/posts/{post}',[ClubPostController::class,'update']);
    Route::delete('clubs/{club}/posts/{post}',[ClubPostController::class,'destroy']);

    Route::post('clubs/{club}/posts/{post}/replies',[ClubPostReplyController::class,'store']);
    Route::post('clubs/{club}/posts/{post}/replies/{reply}',[ClubPostReplyController::class,'store_to_reply']);
    Route::get('clubs/{club}/posts/{post}/replies/{reply}',[ClubPostReplyController::class,'show']);
    Route::put('clubs/{club}/posts/{post}/replies/{reply}',[ClubPostReplyController::class,'update']);
    Route::delete('clubs/{club}/posts/{post}/replies/{reply}',[ClubPostReplyController::class,'destroy']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('requests/friends',[FriendRequestController::class,'index']);
    Route::get('requests/friends/received_requests',[FriendRequestController::class,'received_requests']);
    Route::get('requests/friends/sended_requests',[FriendRequestController::class,'sended_requests']);
    Route::post('requests/friends',[FriendRequestController::class,'store']);
    Route::get('requests/friends/{request}',[FriendRequestController::class,'show']);
    Route::delete('requests/friends/{request}',[FriendRequestController::class,'destroy']);

    Route::get('requests/clubs',[ClubRequestController::class,'index']);
    Route::get('requests/clubs/received_requests',[ClubRequestController::class,'received_requests']);
    Route::get('requests/clubs/sended_requests',[ClubRequestController::class,'sended_requests']);
    Route::post('requests/clubs',[ClubRequestController::class,'store']);
    Route::get('requests/clubs/{request}',[ClubRequestController::class,'show']);
    Route::delete('requests/clubs/{request}',[ClubRequestController::class,'destroy']);

    Route::get('requests/books',[BookRequestController::class,'index']);
    Route::post('requests/books',[BookRequestController::class,'store']);
    Route::patch('requests/books/{request}',[BookRequestController::class,'update']);
    Route::get('requests/books/{request}',[BookRequestController::class,'show']);
    Route::delete('requests/books/{request}',[BookRequestController::class,'destroy']);

    Route::get('requests/reports',[ReportController::class,'index']);
    Route::post('requests/reports',[ReportController::class,'store']);
    Route::get('requests/reports/{report}',[ReportController::class,'show']);
    Route::delete('requests/reports/{report}',[ReportController::class,'destroy']);

    Route::get('requests/responses',[ResponseController::class,'index']);
    Route::get('requests/responses/{response}',[ResponseController::class,'show']);

    Route::post('requests/friends/{request}/handle',[RequestHandlerController::class,'friend']);
    Route::post('requests/clubs/{request}/handle',[RequestHandlerController::class,'club']);
    Route::post('requests/books/{request}/handle',[RequestHandlerController::class,'book']);
    Route::post('requests/reports/{report}/handle',[RequestHandlerController::class,'report']);

});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('friends',[FriendController::class,'index']);
    Route::post('friends',[FriendController::class,'store']);
    Route::delete('friends/{friend}',[FriendController::class,'destroy']);
    Route::get('friends/books',[FriendFeedsController::class,'books']);
    Route::get('friends/book_statuses',[FriendFeedsController::class,'book_statuses']);
    Route::get('friends/challenges',[FriendFeedsController::class,'challenges']);
    Route::get('friends/friends_suggests',[FriendFeedsController::class,'friends_suggests']);

});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('search/tags',[SearchController::class,'searchByTags']);
    Route::prefix('/for-you')->group(function(){
        Route::get('/tags/{tag}/books',[ForYouController::class,'booksByTags']);
    });
});


// haven't been tested
Route::middleware(['auth:sanctum'])->group(function(){
    Route::get('books/{book}/file',[BookController::class,'getFile']);
    Route::get('books/{book}/file/notes',[BookNoteController::class,'index']);
    Route::post('books/{book}/file/notes',[BookNoteController::class,'store']);
    Route::get('books/{book}/file/notes/{note}',[BookNoteController::class,'show']);
    Route::put('books/{book}/file/notes/{note}',[BookNoteController::class,'update']);
    Route::delete('books/{book}/file/notes/{note}',[BookNoteController::class,'destroy']);

    Route::get('books/{book}/file/bookmarks',[BookmarksController::class,'index']);
    Route::post('books/{book}/file/bookmarks',[BookmarksController::class,'store']);
    Route::delete('books/{book}/file/bookmarks/{bookmark}',[BookmarksController::class,'destroy']);
});

Route::get('search/top_5_tags_most_rated_in_past_12_months',[FilterController::class,'top_5_tags_most_rated_in_past_12_months']);
Route::get('search/number_of_new_users_each_week_in_month',[FilterController::class,'number_of_new_users_each_week_in_month']);
Route::get('search/number_of_reports_each_week_in_month',[FilterController::class,'number_of_reports_each_week_in_month']);
Route::get('search/number_of_logout_users_each_week_in_month',[FilterController::class,'number_of_logout_users_each_week_in_month']);
Route::get('search/number_of_users_who_rate_book_each_week_in_month',[FilterController::class,'number_of_users_who_rate_book_each_week_in_month']);
Route::get('search/number_of_users_who_completed_challenges_each_week_in_month',[FilterController::class,'number_of_users_who_completed_challenges_each_week_in_month']);
Route::get('search/percentage_tags',[FilterController::class,'percentage_tags']);

Route::get('sho',function(){


});
